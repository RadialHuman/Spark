import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StructField, StructType, DoubleType, StringType, DateType}
// 31. Spark SQL Part 4 (using Scala)

object sql6 {
  def main(s: Array[String]): Unit = {

    //  val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]") not anymore
    val spark = SparkSession.builder().master("local[*]").appName("ssql").getOrCreate()
    import spark.implicits._ // useful stuff

    spark.sparkContext.setLogLevel("WARN") // less verbose

    // since the schema is all messed up, we need to create sschemas for all the columns we need
    // StructType defines the rows and thier types in the dataframe
    val tsch = StructType(Array(
      StructField("sId",StringType),
      StructField("date",DateType), // the date format is not the standard in the input, intimate the user
      StructField("measureType",StringType),
      StructField("value",DoubleType)
    ) )
    // reading command changes here to define the schema
    // option is used to represent the format in which the input field is
    val data17 = spark.read.schema(tsch).option("dateFormat","yyyyMMdd").csv("E:\\Concepts\\Spark\\Data_sets\\2017.csv")
    // data17.show()
    // data17.schema.printTreeString()

    // this is added for this video - EDA
    // filtering for just TMAX would have taken filter command in RDD
    // but in DFor DS, this si eperimental for rows, so we go for where like in SQL
    // this takes columns wise conditions
    val tmax17 = data17.filter($"measureType"==="TMAX")
    val tmin17 = data17.filter('measureType==="TMIN")  // this si smilar but uses scala symbol to column
    tmax17.show()
    tmin17.show()
    // next video would be in perfmroniang operatiosn on tables

    spark.stop()
  }
}

/*
+-----------+----------+-----------+-----+
|        sId|      date|measureType|value|
+-----------+----------+-----------+-----+
|ASN00015643|2017-01-01|       TMAX|274.0|
|ASN00085296|2017-01-01|       TMAX|217.0|
|ASN00085280|2017-01-01|       TMAX|215.0|
|ASN00040209|2017-01-01|       TMAX|293.0|
|ASN00068151|2017-01-01|       TMAX|254.0|
|USW00024061|2017-01-01|       TMAX|-77.0|
|USW00024229|2017-01-01|       TMAX| 44.0|
|USW00094626|2017-01-01|       TMAX| 11.0|
|USW00012876|2017-01-01|       TMAX|278.0|
|USW00014719|2017-01-01|       TMAX| 89.0|
|USW00004131|2017-01-01|       TMAX|-85.0|
|USW00012842|2017-01-01|       TMAX|272.0|
|USW00003967|2017-01-01|       TMAX| 72.0|
|USS0018F01S|2017-01-01|       TMAX|-34.0|
|USS0019L03S|2017-01-01|       TMAX| 41.0|
|USW00003048|2017-01-01|       TMAX|105.0|
|USW00003889|2017-01-01|       TMAX|100.0|
|USS0014G01S|2017-01-01|       TMAX|-42.0|
|USS0017B04S|2017-01-01|       TMAX|-62.0|
|USS0014C04S|2017-01-01|       TMAX|-68.0|
+-----------+----------+-----------+-----+
only showing top 20 rows

+-----------+----------+-----------+------+
|        sId|      date|measureType| value|
+-----------+----------+-----------+------+
|ASN00015643|2017-01-01|       TMIN| 218.0|
|ASN00085296|2017-01-01|       TMIN| 127.0|
|ASN00085280|2017-01-01|       TMIN| 156.0|
|ASN00040209|2017-01-01|       TMIN| 250.0|
|ASN00068151|2017-01-01|       TMIN| 200.0|
|USW00024061|2017-01-01|       TMIN|-205.0|
|USW00024229|2017-01-01|       TMIN|   0.0|
|USW00094626|2017-01-01|       TMIN| -88.0|
|USW00012876|2017-01-01|       TMIN| 128.0|
|USW00014719|2017-01-01|       TMIN| -66.0|
|USW00004131|2017-01-01|       TMIN|-265.0|
|USW00012842|2017-01-01|       TMIN| 150.0|
|USW00003967|2017-01-01|       TMIN| -55.0|
|USS0018F01S|2017-01-01|       TMIN|-115.0|
|USS0019L03S|2017-01-01|       TMIN|-102.0|
|USW00003048|2017-01-01|       TMIN|   6.0|
|USW00003889|2017-01-01|       TMIN|  39.0|
|USS0014G01S|2017-01-01|       TMIN|-107.0|
|USS0017B04S|2017-01-01|       TMIN|-128.0|
|USS0014C04S|2017-01-01|       TMIN|-185.0|
+-----------+----------+-----------+------+
only showing top 20 rows

 */