import org.apache.spark.SparkConf
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types.{DateType, DoubleType, StringType, StructField, StructType}
// 35. Spark SQL Part 8  (using Scala)

object sql10 {
  def main(s: Array[String]): Unit = {

    //  val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]") not anymore
    val spark = SparkSession.builder().master("local[*]").appName("ssql").getOrCreate()
    import spark.implicits._ // useful stuff

    spark.sparkContext.setLogLevel("WARN") // less verbose


    val tsch = StructType(Array(
      StructField("sId",StringType),
      StructField("date",DateType), // the date format is not the standard in the input, intimate the user
      StructField("measureType",StringType),
      StructField("value",DoubleType)
    ) )

    val data17 = spark.read.schema(tsch).option("dateFormat","yyyyMMdd").csv("E:\\Concepts\\Spark\\Data_sets\\2017.csv")

    val tmax17 = data17.filter($"measureType"==="TMAX").limit(100000).drop("measureType").withColumnRenamed("value","TMAX")
    val tmin17 = data17.filter('measureType==="TMIN").limit(100000).drop("measureType").withColumnRenamed("value","TMIN")
    //val combo17 = tmax17.join(tmin17, tmax17("sId") === tmin17("sId") && tmax17("date") === tmin17("date"))
    // combo17.show()


    val combo17 = tmax17.join(tmin17, Seq("sId","date")) // by default is an inner join, so uncommon columns/rows will be thrown away

    // for average between two columns
    val avg17 = combo17.select('sId,'date,('TMAX+'TMIN)/2).withColumnRenamed("((TMAX + TMIN) / 2)","TAVE")
   avg17.show()

    val new_schema = StructType(Array(
      StructField("sId",StringType),
      StructField("lat",DoubleType),
      StructField("lon",DoubleType),
      StructField("name",StringType)

    ))
    // gives this in a string format so mapping
    val sRDD = spark.sparkContext.textFile("E:\\Concepts\\Spark\\Data_sets\\ghcnd-stations.txt").map{ l =>
      val id = l.substring(0,11)
      val lat = l.substring(12,20).toDouble
      val lon = l.substring(21,30).toDouble
      val name  = l.substring(41,71)
      Row(id,lat,lon,name) // many rows there so that can be a problem
    }

    // convertign it to DF
    val stations = spark.createDataFrame(sRDD,new_schema).cache()
    stations.show()

    // for station wise averages, group by returns a relational groupby dataset, agg is from the functions which has to be manually imported
    import org.apache.spark.sql.functions._ // an object and not a package so no auto import
    val stationTemp17 = avg17.groupBy('sId).agg(avg('TAVE))
    stationTemp17.show()

    // now to join eveyrthign we need
    val joined17 = stationTemp17.join(stations,"sId")
    joined17.show()


    spark.stop()
  }
}

/*
java.lang.NullPointerException
 */