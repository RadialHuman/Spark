import org.apache.spark.SparkConf
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types.{DateType, DoubleType, StringType, StructField, StructType}
// 38. Spark SQL with SQL Part 2 (using Scala)

// SPark SQL also has an SQL component, which can interact directly with SQL
// where can be used istead of filter, join and other things
// Direct sql codes is also possible

object sql12 {
  def main(s: Array[String]): Unit = {

    //  val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]") not anymore
    val spark = SparkSession.builder().master("local[*]").appName("ssql").getOrCreate()
    import spark.implicits._ // useful stuff

    spark.sparkContext.setLogLevel("WARN") // less verbose


    val tsch = StructType(Array(
      StructField("sId",StringType),
      StructField("date",DateType), // the date format is not the standard in the input, intimate the user
      StructField("measureType",StringType),
      StructField("value",DoubleType)
    ) )

    val data17 = spark.read.schema(tsch).option("dateFormat","yyyyMMdd").csv("E:\\Concepts\\Spark\\Data_sets\\2017.csv")

    val tmax17 = data17.filter($"measureType"==="TMAX").limit(100000).drop("measureType").withColumnRenamed("value","TMAX")
    val tmin17 = data17.filter('measureType==="TMIN").limit(100000).drop("measureType").withColumnRenamed("value","TMIN")
    //val combo17 = tmax17.join(tmin17, tmax17("sId") === tmin17("sId") && tmax17("date") === tmin17("date"))
    // combo17.show()


    val combo17 = tmax17.join(tmin17, Seq("sId","date")) // by default is an inner join, so uncommon columns/rows will be thrown away

    // for average between two columns
    val avg17 = combo17.select('sId,'date,('TMAX+'TMIN)/2).withColumnRenamed("((TMAX + TMIN) / 2)","TAVE")
    // avg17.show()

    val new_schema = StructType(Array(
      StructField("sId",StringType),
      StructField("lat",DoubleType),
      StructField("lon",DoubleType),
      StructField("name",StringType)

    ))
    // gives this in a string format so mapping
    val sRDD = spark.sparkContext.textFile("E:\\Concepts\\Spark\\Data_sets\\ghcnd-stations.txt").map{ l =>
      val id = l.substring(0,11)
      val lat = l.substring(12,20).toDouble
      val lon = l.substring(21,30).toDouble
      val name  = l.substring(41,71)
      Row(id,lat,lon,name) // many rows there so that can be a problem
    }

    // convertign it to DF
    val stations = spark.createDataFrame(sRDD,new_schema).cache()

    // *********************************************************************************************************
    // to get avg daily temp
    // stations.show()
    // SQL query can be added
    // registering the df or ds or rdd which needs to be operated in sql
    data17.createOrReplaceTempView("data17") // view for sql
    // filtering out only tmax, from data17 , but sql does not know the existence of this dataframe or rdd
    // limit is required sue to standalone spark system
    // sql is not case sensitive
    // Databricks has sql reference for syntax
    val sqlOnly = spark.sql(
      """
       Select sId, date, (TMAX+TMIN)/2 as TAVE
         from

        (Select sId, date, value as TMAX  from data17 where measureType="TMAX" limit 1000)
        JOIN
        (Select sId, date, value as TMIN  from data17 where measureType="TMIN" limit 1000)
        USING (sId,date)

      """.stripMargin)

    // further analysis on these ensted selects, can be also done by nested them more or
    // can be done by making the sqlOnly register as view and operating on them
    sqlOnly.show()




    spark.stop()
  }
}

/*

+-----------+----------+------+
|        sId|      date|  TAVE|
+-----------+----------+------+
|ASN00015643|2017-01-01| 246.0|
|ASN00085296|2017-01-01| 172.0|
|ASN00085280|2017-01-01| 185.5|
|ASN00040209|2017-01-01| 271.5|
|ASN00068151|2017-01-01| 227.0|
|USW00024061|2017-01-01|-141.0|
|USW00024229|2017-01-01|  22.0|
|USW00094626|2017-01-01| -38.5|
|USW00012876|2017-01-01| 203.0|
|USW00014719|2017-01-01|  11.5|
|USW00004131|2017-01-01|-175.0|
|USW00012842|2017-01-01| 211.0|
|USW00003967|2017-01-01|   8.5|
|USS0018F01S|2017-01-01| -74.5|
|USS0019L03S|2017-01-01| -30.5|
|USW00003048|2017-01-01|  55.5|
|USW00003889|2017-01-01|  69.5|
|USS0014G01S|2017-01-01| -74.5|
|USS0017B04S|2017-01-01| -95.0|
|USS0014C04S|2017-01-01|-126.5|
+-----------+----------+------+
only showing top 20 rows
 */