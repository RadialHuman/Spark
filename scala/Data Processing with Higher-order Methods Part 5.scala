//6. Data Processing with Higher-order Methods Part 5 (Scala)
// Higher order methods to do some analysis
// find number of days that have more precipitation than 1

case class TempDataCC6(day:Int, jd:Int, month:Int, year:Int, precip:Double, snow:Double, avgT:Int, maxT:Int, minT:Int)

object TempData6 { // for precipitation and snow fall to convert "." to -1
  def toDoubleOrNeg(s:String):Double={
    try{
      s.toDouble
    }
    catch{
      case  _:NumberFormatException => -1
    }
  }

  def main(s:Array[String]):Unit={
    val source = scala.io.Source.fromFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv")
    val lines = source.getLines()
    //println("The number of rows before cleaning %d ", lines.length)
    val data = lines.drop(1)
    val rows  = data.flatMap{ l=> // this is where this one differs from the previous one, flatmap would take in a seq
      val p = l.split(",")
      if(p(6)=="."||p(7)=="."||p(8)==".") Seq.empty
      else
        Seq(TempDataCC(p(0).toInt,p(1).toInt,p(2).toInt,p(3).toInt,toDoubleOrNeg(p(4)),toDoubleOrNeg(p(5)),p(6).toInt,p(7).toInt,p(8).toInt))
    }.toArray
    source.close()

    // for all those rainy days, whats the lowest temp
    // filter rainy days and map them by thier high temp and sum them, this is not efficient and more space consuming
    // reduce can be sued to combine but uses only one type
    // fold is another type as in the previous one, but this one si flatmap
    // map and filter at the same time, without duplicating
    // flatmap is used to create a sequence with the applied function
    // runs once, and gets output in  collections and converts all the colleciton into a big collection
    // advantageous
    val rainyTemp = rows.flatMap(td => if(td.precip < 1.0) Seq.empty else Seq(td.maxT) )
    println(s"This is still the same output of average temp during rainy  ${rainyTemp.sum/rainyTemp.length} ")
  }
}

/*

 */

