import org.apache.spark.SparkConf
import org.apache.spark.sql.{Row, SparkSession, Encoders}
import org.apache.spark.sql.types.{DateType, DoubleType, StringType, StructField, StructType}

// 39 and 40. Spark SQL- Typed Datasets Part 1 and 2(using Scala)

// Dataframes till now were good like sql, but Datasets are more than just row
// Typed Dataset
// In the api website, these functions have the [T] notation
// in the typed transforms output is a dataset as either [U]or [T]
// while untyped output is dataframe
// most of these are experimental
// The problem is that lambda functions used in this can be opaque to Spark and so cant optmize unlike sql

case class Series (id: String, area: String, measure:String, title: String)
case class ldata (id: String, year: Int, period: String, value:Double)


object ds1 {
  def main(s: Array[String]): Unit = {
    // ******************************************************************************************************
    //  val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]") not anymore
    // val spark = SparkSession.builder().master("local[*]").appName("ssql").getOrCreate()
    // import spark.implicits._ // useful stuff

    // spark.sparkContext.setLogLevel("WARN") // less verbose

    // using la.series and ls.county for operations
    // zipc code will give latitue and longi of all the zip in us
    // ******************************************************************************************************
    // loading different type fo files like tsv and csv so a case class for all the datasets to load

    // case class zip

    val spark = SparkSession.builder().master("local[*]").getOrCreate()
    import spark.implicits._

    spark.sparkContext.setLogLevel("WARN")
    // in rdd, readin data was at our discresstion, any column could have been taken and rejected ex: p(2)
    // but in dataset, its not like that

    val cData  = spark.read.option("header",true).option("delimiter","\t").csv("E:\\Concepts\\Spark\\Data_sets\\la_series.txt")
    cData.show()

    // no typed aspect and cData is right now a dataframe, to do so,

    //val cDataS  = spark.read.option("header",true).option("delimiter","\t")
                  //.csv("E:\\Concepts\\Spark\\Data_sets\\la_series.txt")
                  //.as[ldata] // passing type fo the result, since the case class apssed doesn account for all the columns it wont work
                  // to make it work there has to be a schema

    val cDataS = spark.read.schema(Encoders.product[ldata].schema).option("header",true).option("delimiter","\t")
                .csv("E:\\Concepts\\Spark\\Data_sets\\la_county.txt")
                .as[ldata]  // make sure the case class is outside the main function
    // case class can be passed to build schema

    cDataS.show()




    spark.stop()
  }
}

/*

+--------------------+--------------+---------------+------------+--------+--------+--------------------+--------------+----------+------------+--------+----------+
|           series_id|area_type_code|      area_code|measure_code|seasonal|srd_code|        series_title|footnote_codes|begin_year|begin_period|end_year|end_period|
+--------------------+--------------+---------------+------------+--------+--------+--------------------+--------------+----------+------------+--------+----------+
|LASBS060000000000...|             L|BS0600000000000|          03|       S|      06|Unemployment Rate...|              |      1976|         M01|    2018|       M01|
|LASBS060000000000...|             L|BS0600000000000|          04|       S|      06|Unemployment: Bal...|              |      1976|         M01|    2018|       M01|
|LASBS060000000000...|             L|BS0600000000000|          05|       S|      06|Employment: Balan...|              |      1976|         M01|    2018|       M01|
|LASBS060000000000...|             L|BS0600000000000|          06|       S|      06|Labor Force: Bala...|              |      1976|         M01|    2018|       M01|
|LASBS120000000000...|             L|BS1200000000000|          03|       S|      12|Unemployment Rate...|              |      1990|         M01|    2018|       M01|
|LASBS120000000000...|             L|BS1200000000000|          04|       S|      12|Unemployment: Bal...|              |      1990|         M01|    2018|       M01|
|LASBS120000000000...|             L|BS1200000000000|          05|       S|      12|Employment: Balan...|              |      1990|         M01|    2018|       M01|
|LASBS120000000000...|             L|BS1200000000000|          06|       S|      12|Labor Force: Bala...|              |      1990|         M01|    2018|       M01|
|LASBS170000000000...|             L|BS1700000000000|          03|       S|      17|Unemployment Rate...|              |      1994|         M01|    2018|       M01|
|LASBS170000000000...|             L|BS1700000000000|          04|       S|      17|Unemployment: Bal...|              |      1994|         M01|    2018|       M01|
|LASBS170000000000...|             L|BS1700000000000|          05|       S|      17|Employment: Balan...|              |      1994|         M01|    2018|       M01|
|LASBS170000000000...|             L|BS1700000000000|          06|       S|      17|Labor Force: Bala...|              |      1994|         M01|    2018|       M01|
|LASBS260000000000...|             L|BS2600000000000|          03|       S|      26|Unemployment Rate...|              |      1990|         M01|    2018|       M01|
|LASBS260000000000...|             L|BS2600000000000|          04|       S|      26|Unemployment: Bal...|              |      1990|         M01|    2018|       M01|
|LASBS260000000000...|             L|BS2600000000000|          05|       S|      26|Employment: Balan...|              |      1990|         M01|    2018|       M01|
|LASBS260000000000...|             L|BS2600000000000|          06|       S|      26|Labor Force: Bala...|              |      1990|         M01|    2018|       M01|
|LASBS360000000000...|             L|BS3600000000000|          03|       S|      36|Unemployment Rate...|              |      1976|         M01|    2018|       M01|
|LASBS360000000000...|             L|BS3600000000000|          04|       S|      36|Unemployment: Bal...|              |      1976|         M01|    2018|       M01|
|LASBS360000000000...|             L|BS3600000000000|          05|       S|      36|Employment: Balan...|              |      1976|         M01|    2018|       M01|
|LASBS360000000000...|             L|BS3600000000000|          06|       S|      36|Labor Force: Bala...|              |      1976|         M01|    2018|       M01|
+--------------------+--------------+---------------+------------+--------+--------+--------------------+--------------+----------+------------+--------+----------+
only showing top 20 rows

+--------------------+----+------+-----+
|                  id|year|period|value|
+--------------------+----+------+-----+
|LAUCN010010000000...|1990|   M01|  6.4|
|LAUCN010010000000...|1990|   M02|  6.6|
|LAUCN010010000000...|1990|   M03|  5.8|
|LAUCN010010000000...|1990|   M04|  6.6|
|LAUCN010010000000...|1990|   M05|  6.0|
|LAUCN010010000000...|1990|   M06|  7.0|
|LAUCN010010000000...|1990|   M07|  6.0|
|LAUCN010010000000...|1990|   M08|  6.7|
|LAUCN010010000000...|1990|   M09|  7.2|
|LAUCN010010000000...|1990|   M10|  7.1|
|LAUCN010010000000...|1990|   M11|  6.2|
|LAUCN010010000000...|1990|   M12|  6.2|
|LAUCN010010000000...|1990|   M13|  6.5|
|LAUCN010010000000...|1991|   M01|  6.7|
|LAUCN010010000000...|1991|   M02|  7.8|
|LAUCN010010000000...|1991|   M03|  7.3|
|LAUCN010010000000...|1991|   M04|  6.8|
|LAUCN010010000000...|1991|   M05|  6.7|
|LAUCN010010000000...|1991|   M06|  7.8|
|LAUCN010010000000...|1991|   M07|  6.8|
+--------------------+----+------+-----+
only showing top 20 rows

 */