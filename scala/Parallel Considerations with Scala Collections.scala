// to see how parallel works

object para extends App {
  val a  = List(1,2,3,4,5,6)
  println("foldLeft + ", a.foldLeft(0)(_+_))
  println("foldRight + ", a.foldRight(0)(_+_))
  println("foldLeft - ", a.foldLeft(0)(_-_))
  println("foldRight - ", a.foldRight(0)(_-_))
  val b  = List(1,2,3,4,5,6).par // parallel
  println("fold parallel - ", b.fold(0)(_-_)) // this produces 2 different values every time its executed,
                                              // as the array size increses, and it works on multiple threads,
                                              // different outcomes is possible
                                              // the function passed has to be ***associative***
  println("fold parallel + ", b.fold(0)(_+_)) // the use of generic fold is limited when it comes to types (subtype)

  // enter aggregate, flexibility of type and parallel fold
  println("Aggregate + +", b.aggregate(0)(_+_,_+_))
  println("Aggregate - -", b.aggregate(0)(_-_,_-_))
  println("Aggregate + -", b.aggregate(0)(_+_,_-_))
  println("Aggregate - +", b.aggregate(0)(_-_,_+_))
}

/*
(foldLeft + ,21)
(foldRight + ,21)
(foldLeft - ,-21)
(foldRight - ,-3)
(fold parallel - ,3)
(fold parallel + ,21)
(Aggregate + +,21)
(Aggregate - -,-3)
(Aggregate + -,-9)
(Aggregate - +,-21)
 */