import org.apache.spark.SparkConf
import org.apache.spark.sql.{Row, SparkSession, Encoders}
import org.apache.spark.sql.types.{DateType, DoubleType, StringType, StructField, StructType}

// 41. Spark SQL- Typed Datasets Part 3(using Scala)


case class Series2 (id: String, area: String, measure:String, title: String)
case class ldata (id: String, year: Int, period: String, value:Double)

// to read the series data, either the series case class has to be updated or like in rdd we can read in the lines as txt
// and map it manually

object ds2 {
  def main(s: Array[String]): Unit = {


    val spark = SparkSession.builder().master("local[*]").getOrCreate()
    import spark.implicits._

    spark.sparkContext.setLogLevel("WARN")


    // val cData  = spark.read.option("header",true).option("delimiter","\t").csv("E:\\Concepts\\Spark\\Data_sets\\la_series.txt")
    // cData.show()

    // option one, read and give a schema with options
    val cDataS = spark.read.schema(Encoders.product[ldata].schema).option("header",true).option("delimiter","\t")
                .csv("E:\\Concepts\\Spark\\Data_sets\\la_county.txt")
                .as[ldata]
                .cache()

    // option two, read in as a textfile, get dataset of strings, then convert to dataset of series case class
    val series = spark.read.textFile("E:\\Concepts\\Spark\\Data_sets\\la_series.txt").map{ line =>
      val p = line.split("\t").map(_.trim)
      Series2(p(0),p(2),p(3),p(6))
    }.cache()



    cDataS.show()
    series.show()



    spark.stop()
  }
}

/*

+--------------------+----+------+-----+
|                  id|year|period|value|
+--------------------+----+------+-----+
|LAUCN010010000000...|1990|   M01|  6.4|
|LAUCN010010000000...|1990|   M02|  6.6|
|LAUCN010010000000...|1990|   M03|  5.8|
|LAUCN010010000000...|1990|   M04|  6.6|
|LAUCN010010000000...|1990|   M05|  6.0|
|LAUCN010010000000...|1990|   M06|  7.0|
|LAUCN010010000000...|1990|   M07|  6.0|
|LAUCN010010000000...|1990|   M08|  6.7|
|LAUCN010010000000...|1990|   M09|  7.2|
|LAUCN010010000000...|1990|   M10|  7.1|
|LAUCN010010000000...|1990|   M11|  6.2|
|LAUCN010010000000...|1990|   M12|  6.2|
|LAUCN010010000000...|1990|   M13|  6.5|
|LAUCN010010000000...|1991|   M01|  6.7|
|LAUCN010010000000...|1991|   M02|  7.8|
|LAUCN010010000000...|1991|   M03|  7.3|
|LAUCN010010000000...|1991|   M04|  6.8|
|LAUCN010010000000...|1991|   M05|  6.7|
|LAUCN010010000000...|1991|   M06|  7.8|
|LAUCN010010000000...|1991|   M07|  6.8|
+--------------------+----+------+-----+
only showing top 20 rows

18/03/25 10:17:46 WARN Executor: 1 block locks were not released by TID = 4:
[rdd_15_0]
+--------------------+---------------+------------+--------------------+
|                  id|           area|     measure|               title|
+--------------------+---------------+------------+--------------------+
|           series_id|      area_code|measure_code|        series_title|
|LASBS060000000000003|BS0600000000000|          03|Unemployment Rate...|
|LASBS060000000000004|BS0600000000000|          04|Unemployment: Bal...|
|LASBS060000000000005|BS0600000000000|          05|Employment: Balan...|
|LASBS060000000000006|BS0600000000000|          06|Labor Force: Bala...|
|LASBS120000000000003|BS1200000000000|          03|Unemployment Rate...|
|LASBS120000000000004|BS1200000000000|          04|Unemployment: Bal...|
|LASBS120000000000005|BS1200000000000|          05|Employment: Balan...|
|LASBS120000000000006|BS1200000000000|          06|Labor Force: Bala...|
|LASBS170000000000003|BS1700000000000|          03|Unemployment Rate...|
|LASBS170000000000004|BS1700000000000|          04|Unemployment: Bal...|
|LASBS170000000000005|BS1700000000000|          05|Employment: Balan...|
|LASBS170000000000006|BS1700000000000|          06|Labor Force: Bala...|
|LASBS260000000000003|BS2600000000000|          03|Unemployment Rate...|
|LASBS260000000000004|BS2600000000000|          04|Unemployment: Bal...|
|LASBS260000000000005|BS2600000000000|          05|Employment: Balan...|
|LASBS260000000000006|BS2600000000000|          06|Labor Force: Bala...|
|LASBS360000000000003|BS3600000000000|          03|Unemployment Rate...|
|LASBS360000000000004|BS3600000000000|          04|Unemployment: Bal...|
|LASBS360000000000005|BS3600000000000|          05|Employment: Balan...|
+--------------------+---------------+------------+--------------------+
only showing top 20 rows


 */