//2. Data Processing with Higher-order Methods Part 2 (Scala)
// from the previous one, missing data is a problem. To solve it
// 1. lines are to be read line/line and dots should be processed or
// 2. lines with these dots can be eliminated <- focus
case class TempDataCC2(day:Int, jd:Int, month:Int, year:Int, precip:Double, snow:Double, avgT:Int, maxT:Int, minT:Int)

object TempData2 {
  def main(s:Array[String]):Unit={
    val source = scala.io.Source.fromFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv")
    val lines = source.getLines()
    println("The number of rows before cleaning %d ", lines.length)
    val data = lines.drop(1)
    val rows  = data.filterNot(_.contains(",.")).map{ l=> // this is where this one differs from the previous one
      val p = l.split(",")
      TempDataCC(p(0).toInt,p(1).toInt,p(2).toInt,p(3).toInt,p(4).toDouble,p(5).toDouble,p(6).toInt,p(7).toInt,p(8).toInt)
    }.toArray
    source.close()
    println("The number of rows after cleaning %d ", rows.length)
    rows.take(5) foreach println

  }
}

/*
(The number of rows before cleaning %d ,42040)
(The number of rows after cleaning %d ,33286)
TempDataCC(1,335,12,1895,0.0,0.0,12,26,-2)
TempDataCC(2,336,12,1895,0.0,0.0,-3,11,-16)
TempDataCC(3,337,12,1895,0.0,0.0,6,11,0)
TempDataCC(4,338,12,1895,0.0,0.0,2,12,-9)
TempDataCC(5,339,12,1895,0.0,0.0,19,26,11)
 */

