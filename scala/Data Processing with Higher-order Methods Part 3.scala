//3. Data Processing with Higher-order Methods Part 3 (Scala)
//Higher order methods to do some analysis
// find highest temp and when

case class TempDataCC4(day:Int, jd:Int, month:Int, year:Int, precip:Double, snow:Double, avgT:Int, maxT:Int, minT:Int)

object TempData4 { // for precipitation and snow fall to convert "." to -1
  def toDoubleOrNeg(s:String):Double={
    try{
      s.toDouble
    }
    catch{
      case  _:NumberFormatException => -1
    }
  }

  def main(s:Array[String]):Unit={
    val source = scala.io.Source.fromFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv")
    val lines = source.getLines()
    //println("The number of rows before cleaning %d ", lines.length)
    val data = lines.drop(1)
    val rows  = data.flatMap{ l=> // this is where this one differs from the previous one, flatmap would take in a seq
      val p = l.split(",")
      if(p(6)=="."||p(7)=="."||p(8)==".") Seq.empty
      else
        Seq(TempDataCC(p(0).toInt,p(1).toInt,p(2).toInt,p(3).toInt,toDoubleOrNeg(p(4)),toDoubleOrNeg(p(5)),p(6).toInt,p(7).toInt,p(8).toInt))
    }.toArray
    source.close()

    // to find the max temp
    val maxT = rows.map(_.maxT).max
    // to find which days (this one is inefficient as the data had to be passed 3 times and map makes a new array and only to get max, which is not good)
    val hotDays = rows.filter(_.maxT == maxT)
    println("on the following days, the temp was highest - %d F",maxT )
    println(hotDays) // this will give back an array like [LTempDataCC;@6c49835d
    println(hotDays.mkString("  "))

    // efficiency in scala might be different from spark collection - single pass
    val hotDays2 = rows.maxBy(_.maxT)
    println("MAXBY - The hot days are %d" , hotDays2)

    // reduce style - single pass
    val hotDays3 = rows.reduceLeft((d1,d2)=> if (d1.maxT<d2.maxT) d2 else d1)
    println("REDUCE - The hot days are %d",hotDays3)
  }
}

/*
(on the following days, the temp was highest - %d F,107)
[LTempDataCC;@6c49835d
TempDataCC(7,189,7,1936,0.0,0.0,89,107,70)  TempDataCC(11,193,7,1936,0.0,0.0,93,107,78)
(MAXBY - The hot days are %d,TempDataCC(7,189,7,1936,0.0,0.0,89,107,70))
(REDUCE - The hot days are %d,TempDataCC(7,189,7,1936,0.0,0.0,89,107,70))
 */

