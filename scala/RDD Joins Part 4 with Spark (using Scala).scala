import org.apache.spark.{SparkConf, SparkContext}
// 26 and 27. RDD Joins Part 4 and 5 with Spark (using Scala)
// operations on the read files

// case class for la_area,la_mino and la_series
// case class Area (code: String, text_desc: String)
// case class Mino (id: String, year: Int, period:Int, value: Double)
// case class Series (id: String, area: String, measure:String, title: String)


object unemp3 {
  def main(s: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("unemp").setMaster("local[*]")
    val sc = new SparkContext(conf)


    sc.setLogLevel("WARN")


    // read the first data to be joined, taking only the two columns that required
    val areas = sc.textFile("E:\\Concepts\\Spark\\Data_sets\\la_area.txt").filter(!_.contains("area_type"))
      .map { lines =>
        val p = lines.split("\t")
          .map(_.trim)
        Area(p(1), p(2))
      }.cache()
    // areas.take(5) foreach println

    // read the second data to be joined, reading all the columns
    val ser = sc.textFile("E:\\Concepts\\Spark\\Data_sets\\la_series.txt").filter(!_.contains("area_code"))
      .map { lines =>
        val p = lines.split("\t")
          .map(_.trim)
        Series(p(0), p(2), p(3), p(6))
      }.cache()
    //ser.take(5) foreach println

    // read in the real data
    val data = sc.textFile("E:\\Concepts\\Spark\\Data_sets\\la_mino.txt").filter(!_.contains("year"))
      .map { lines =>
        val p = lines.split("\t")
          .map(_.trim)
        Mino(p(0), p(1).toInt , p(2).drop(1).toInt, p(3).toDouble)
      }.cache()
    // data.take(5) foreach println



    // problem - find areas and series that have higher avg unempl rate in 90s than previous decades

    // break down decade wise
    // but the rate column has numbers as well as rates
    // unempl rate's codes end with "03"
    // create paired rdds

    val rates = data.filter(_.id.endsWith("03"))  // for unemployed only
    val decadeGroups = rates.map(d => (d.id, d.year/10) -> d.value)  // kvp with k as a tuple
    val decadeAvg = decadeGroups.aggregateByKey(0.0 -> 0 )({ case ((s,c),d)=> (s+d,c+1)},{ case ((s1,c1),(s2,c2)) => (s1+s2,c1+c2)}).mapValues(t => t._1/t._2) // added later
    // decadeAvg.take(5) foreach println

    // the output is all random so cant be addedd withouth sort
    // there is a decade thing , sum and count to get the average
    // rest in the next  code
    // now this code, there is a great jump betwwen the previous and this one especially the added later ones

    //added later, in this one, the maximum rate in a decade is calcuated
    val maxDec = decadeAvg.map{ case ((id,dec),av) => id -> (dec*10,av)}
                              .reduceByKey{case ((d1,a1),(d2,a2)) => if(a1 >= a2) (d1,a1) else (d2,a2)}

    val seriesPair = ser.map(s => s.id -> s.title)

    val joinedMaxDec = seriesPair.join(maxDec)

    //joinedMaxDec.take(5) foreach  println

    // this is whats taught in this one
    // area and series have nothign much in common, so a more complex join has to be make
    // area codes are substring of the series in joinedMaxDec

    // first a different set of keys are made, flatten and add araes in them, can use substring too for efficiency
    val dataByArea = joinedMaxDec.mapValues{ case (a,(b,c))=> (a,b,c)}
                  .map{ case (id, t) => (id.drop(3).dropRight(2)) -> t}

    // join can happen with 2 paired rdd but area is not a pair yet
    val fullJoint  =areas.map(a => a.code -> a.text_desc).join(dataByArea) // inner joins might leave out keys that are missing in any one pRDD

    fullJoint.take(10) foreach  println

    sc.stop()

    //
  }
}

/*
skipped part
(LAUCT275585200000003,(Unemployment Rate: Roseville city, MN (U),(2010,4.283653846153845)))
(LAUCT275800000000003,(Unemployment Rate: St. Paul city, MN (U),(2010,5.072115384615387)))
(LAUCT270638200000003,(Unemployment Rate: Blaine city, MN (U),(2010,4.774038461538462)))
(LAUMT274034000000003,(Unemployment Rate: Rochester, MN Metropolitan Statistical Area (U),(2010,4.249999999999999)))
(LAUMC272433000000003,(Unemployment Rate: Grand Rapids, MN Micropolitan Statistical Area (U),(1990,9.79307692307692)))

actual result
(PT2739878013000,(Mankato city, Blue Earth County part, MN,(Unemployment Rate: Mankato city, Blue Earth County part, MN (U),1990,3.3692307692307684)))
(SA2782140000000,(Pennington-Red Lake, MN LMA,(Unemployment Rate: Pennington-Red Lake, MN LMA (U),1990,8.314615384615385)))
(CT2718188000000,(Edina city, MN,(Unemployment Rate: Edina city, MN (U),2010,3.868269230769229)))
(CN2701300000000,(Blue Earth County, MN,(Unemployment Rate: Blue Earth County, MN (U),2010,4.175000000000002)))
(MC2721860000000,(Fairmont, MN Micropolitan Statistical Area,(Unemployment Rate: Fairmont, MN Micropolitan Statistical Area (U),2010,5.185576923076923)))
(CN2709100000000,(Martin County, MN,(Unemployment Rate: Martin County, MN (U),2010,5.185576923076923)))
(CN2705700000000,(Hubbard County, MN,(Unemployment Rate: Hubbard County, MN (U),1990,7.677692307692308)))
(CN2707500000000,(Lake County, MN,(Unemployment Rate: Lake County, MN (U),1990,6.350000000000003)))
(CT2757220000000,(St. Louis Park city, MN,(Unemployment Rate: St. Louis Park city, MN (U),2010,3.935576923076923)))
(MC2735580000000,(New Ulm, MN Micropolitan Statistical Area,(Unemployment Rate: New Ulm, MN Micropolitan Statistical Area (U),2010,5.043269230769233)))
 */