import org.apache.spark.SparkConf
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types.{DateType, DoubleType, StringType, StructField, StructType}
// 37. Spark SQL with SQL Part 1 (using Scala)

// SPark SQL also has an SQL component, which can interact directly with SQL
// where can be used istead of filter, join and other things
// Direct sql codes is also possible

object sql11 {
  def main(s: Array[String]): Unit = {

    //  val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]") not anymore
    val spark = SparkSession.builder().master("local[*]").appName("ssql").getOrCreate()
    import spark.implicits._ // useful stuff

    spark.sparkContext.setLogLevel("WARN") // less verbose


    val tsch = StructType(Array(
      StructField("sId",StringType),
      StructField("date",DateType), // the date format is not the standard in the input, intimate the user
      StructField("measureType",StringType),
      StructField("value",DoubleType)
    ) )

    val data17 = spark.read.schema(tsch).option("dateFormat","yyyyMMdd").csv("E:\\Concepts\\Spark\\Data_sets\\2017.csv")

    val tmax17 = data17.filter($"measureType"==="TMAX").limit(100000).drop("measureType").withColumnRenamed("value","TMAX")
    val tmin17 = data17.filter('measureType==="TMIN").limit(100000).drop("measureType").withColumnRenamed("value","TMIN")
    //val combo17 = tmax17.join(tmin17, tmax17("sId") === tmin17("sId") && tmax17("date") === tmin17("date"))
    // combo17.show()


    val combo17 = tmax17.join(tmin17, Seq("sId","date")) // by default is an inner join, so uncommon columns/rows will be thrown away

    // for average between two columns
    val avg17 = combo17.select('sId,'date,('TMAX+'TMIN)/2).withColumnRenamed("((TMAX + TMIN) / 2)","TAVE")
    // avg17.show()

    val new_schema = StructType(Array(
      StructField("sId",StringType),
      StructField("lat",DoubleType),
      StructField("lon",DoubleType),
      StructField("name",StringType)

    ))
    // gives this in a string format so mapping
    val sRDD = spark.sparkContext.textFile("E:\\Concepts\\Spark\\Data_sets\\ghcnd-stations.txt").map{ l =>
      val id = l.substring(0,11)
      val lat = l.substring(12,20).toDouble
      val lon = l.substring(21,30).toDouble
      val name  = l.substring(41,71)
      Row(id,lat,lon,name) // many rows there so that can be a problem
    }

    // convertign it to DF
    val stations = spark.createDataFrame(sRDD,new_schema).cache()
    // stations.show()
// *********************************************************************************************************
    // SQL query can be added
    // registering the df or ds or rdd which needs to be operated in sql
    data17.createOrReplaceTempView("data17") // view for sql
    // filtering out only tmax, from data17 , but sql does not know the existence of this dataframe or rdd
    val sqlOnly = spark.sql(
      """
        Select sId, date, value as TMAX  from data17 where measureType="TMAX"
      """.stripMargin)
    sqlOnly.show()

    spark.stop()
  }
}

/*
+-----------+----------+-----+
|        sId|      date| TMAX|
+-----------+----------+-----+
|ASN00015643|2017-01-01|274.0|
|ASN00085296|2017-01-01|217.0|
|ASN00085280|2017-01-01|215.0|
|ASN00040209|2017-01-01|293.0|
|ASN00068151|2017-01-01|254.0|
|USW00024061|2017-01-01|-77.0|
|USW00024229|2017-01-01| 44.0|
|USW00094626|2017-01-01| 11.0|
|USW00012876|2017-01-01|278.0|
|USW00014719|2017-01-01| 89.0|
|USW00004131|2017-01-01|-85.0|
|USW00012842|2017-01-01|272.0|
|USW00003967|2017-01-01| 72.0|
|USS0018F01S|2017-01-01|-34.0|
|USS0019L03S|2017-01-01| 41.0|
|USW00003048|2017-01-01|105.0|
|USW00003889|2017-01-01|100.0|
|USS0014G01S|2017-01-01|-42.0|
|USS0017B04S|2017-01-01|-62.0|
|USS0014C04S|2017-01-01|-68.0|
+-----------+----------+-----+
only showing top 20 rows

 */