package org.sample

import org.apache.spark.sql.SparkSession

object word_count {

  def main(args : Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("GitHub push counter")
      .master("local[*]")
      .getOrCreate()

    val sc = spark.sparkContext
    // word count on names used in the state of WY, taken from SOS USA DB for persons name
    val d = sc.textFile("E:\\Projects\\Scala\\datasets\\WY.txt")
      .flatMap(line => line.split(","))
      .map(word => (word, 1))
      .reduceByKey(_ + _)
      .collect
    d.foreach(println)
  }

}