import org.apache.spark.sql.SparkSession
import swiftvis2.plotting
import swiftvis2._
//46 and 47. Spark ML Introduction (using Scala) and 47. Spark ML Clustering Part 1 (using Scala)

/*
There are two ML libraries in Spark
1. org.apache.spark.ml.evaluation <- works with the updated dataset types
2. org.apache.spark.mllib <- works with RDDs

Not everything frmo RDD has been moved to Datasets

ML types:
1. Supervised
2. Unsupervised
3. Reinforcement

This will have Clustering (Unsupervised)
 - KMeans
 Goal si to cluster different climate regions on globe

 */
case class Station(sid:String, lat:Double, lon: Double, ele:Double, name:String)


object ml1 extends JFXApp {
  def main(s:Array[String]):Unit= {

    val spark = SparkSession.builder().master("local[*]").getOrCreate()
    import spark.implicits._

    spark.sparkContext.setLogLevel("WARN")

    // loda in data and cluster it geographically, typed dataset

    val stats = spark.read.textFile("E:\\Concepts\\Spark\\Data_sets\\ghcnd-stations.txt").map{
      line =>
      val id = line.substring(0,11)
      val lat =  line.substring(12,20).trim.toDouble
      val lon = line.substring(21,30).trim.toDouble
      val ele =  line.substring(31,37).trim.toDouble
      val name =  line.substring(41,71)
      Station(id, lat, lon, ele, name)

    }.cache()

    stats.show()

    // to plot them x axis =longitude
    val x  = stats.select('lon).as[Double].collect()
    val y  = stats.select('lat).as[Double].collect()

    // for plotting swiftvis2.plotting <-- skipped
    Plot.scatterPlot(x,y,title = "Stations",xLable= "Longitude", yLabel = "Latitude", symbolSize = 3, symbolColor =Black.ARGB)


    spark.stop()
  }
}


/*
Not graph as no jfx installed

+-----------+-------+--------+------+--------------------+
|        sid|    lat|     lon|   ele|                name|
+-----------+-------+--------+------+--------------------+
|ACW00011604|17.1167|-61.7833|  10.1|ST JOHNS COOLIDGE...|
|ACW00011647|17.1333|-61.7833|  19.2|ST JOHNS         ...|
|AE000041196| 25.333|  55.517|  34.0|SHARJAH INTER. AI...|
|AEM00041194| 25.255|  55.364|  10.4|DUBAI INTL       ...|
|AEM00041217| 24.433|  54.651|  26.8|ABU DHABI INTL   ...|
|AEM00041218| 24.262|  55.609| 264.9|AL AIN INTL      ...|
|AF000040930| 35.317|  69.017|3366.0|NORTH-SALANG     ...|
|AFM00040938|  34.21|  62.228| 977.2|HERAT            ...|
|AFM00040948| 34.566|  69.212|1791.3|KABUL INTL       ...|
|AFM00040990|   31.5|   65.85|1010.0|KANDAHAR AIRPORT ...|
|AG000060390|36.7167|    3.25|  24.0|ALGER-DAR EL BEID...|
|AG000060590|30.5667|  2.8667| 397.0|EL-GOLEA         ...|
|AG000060611|  28.05|  9.6331| 561.0|IN-AMENAS        ...|
|AG000060680|   22.8|  5.4331|1362.0|TAMANRASSET      ...|
|AGE00135039|35.7297|    0.65|  50.0|ORAN-HOPITAL MILI...|
|AGE00147704|  36.97|    7.79| 161.0|ANNABA-CAP DE GAR...|
|AGE00147705|  36.78|    3.07|  59.0|ALGIERS-VILLE/UNI...|
|AGE00147706|   36.8|    3.03| 344.0|ALGIERS-BOUZAREAH...|
|AGE00147707|   36.8|    3.04|  38.0|ALGIERS-CAP CAXIN...|
|AGE00147708|  36.72|    4.05| 222.0|TIZI OUZOU       ...|
+-----------+-------+--------+------+--------------------+
only showing top 20 rows

 */

