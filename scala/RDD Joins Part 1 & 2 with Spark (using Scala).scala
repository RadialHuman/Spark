import org.apache.spark.{SparkConf, SparkContext}
// 23 and 24.  RDD Joins Part 1 and Part 2 with Spark (using Scala)
// https://download.bls.gov/pub/time.series/la/ is the data source
// There is gonna be a join between the two tables, so two case classes are to be made
// all the tables are read and in the next one joins will occur

// case class for la_area,la_mino and la_series
case class Area (code: String, text_desc: String)
case class Mino (id: String, year: Int, period:Int, value: Double)
case class Series (id: String, area: String, measure:String, title: String)


object unemp {
  def main(s: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("unemp").setMaster("local[*]")
    val sc = new SparkContext(conf)


    sc.setLogLevel("WARN")


    // read the first data to be joined, taking only the two columns that required
    val areas = sc.textFile("E:\\Concepts\\Spark\\Data_sets\\la_area.txt").filter(!_.contains("area_type"))
      .map { lines =>
        val p = lines.split("\t")
          .map(_.trim)
        Area(p(1), p(2))
      }.cache()
    // areas.take(5) foreach println

    // read the second data to be joined, reading all the columns
    val ser = sc.textFile("E:\\Concepts\\Spark\\Data_sets\\la_series.txt").filter(!_.contains("area_code"))
      .map { lines =>
        val p = lines.split("\t")
          .map(_.trim)
        Series(p(0), p(2), p(3), p(6))
      }.cache()
    //ser.take(5) foreach println

    // read in the real data
    val data = sc.textFile("E:\\Concepts\\Spark\\Data_sets\\la_mino.txt").filter(!_.contains("year"))
      .map { lines =>
        val p = lines.split("\t")
          .map(_.trim)
        Mino(p(0), p(1).toInt , p(2).drop(1).toInt, p(3).toDouble)
      }.cache()
    data.take(5) foreach println

    sc.stop()
  }
}