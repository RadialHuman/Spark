import org.apache.spark.SparkConf
import org.apache.spark.sql.{Row, SparkSession, Encoders}
import org.apache.spark.sql.types.{DateType, DoubleType, StringType, StructField, StructType}
import org.apache.spark.sql.functions._


// 44. Spark SQL- Typed Datasets Part 6 (using Scala)


// case class Series2 (id: String, area: String, measure:String, title: String)
 case class ldatas (ids: String, year: Int, period: String, value:Double)
case class zipcode(zipCode:String,lat:Double,longi:Double,city:String,state:String,county:String)
case class countyzip(lat:Double,longi:Double,state:String, county:String)

// to read the series data, either the series case class has to be updated or like in rdd we can read in the lines as txt
// and map it manually

object ds4 {
  def main(s: Array[String]): Unit = {


    val spark = SparkSession.builder().master("local[*]").getOrCreate()
    import spark.implicits._

    spark.sparkContext.setLogLevel("WARN")

    // option one, read and give a schema with options
    val cDataS = spark.read.schema(Encoders.product[ldatas].schema).option("header",true).option("delimiter","\t")
                .csv("E:\\Concepts\\Spark\\Data_sets\\la_county.txt")
                .as[ldatas]
                .cache()

    // option two, read in as a textfile, get dataset of strings, then convert to dataset of series case class
    val series = spark.read.textFile("E:\\Concepts\\Spark\\Data_sets\\la_series.txt").map{ line =>
      val p = line.split("\t").map(_.trim)
      Series2(p(0),p(2),p(3),p(6))
    }.cache()



    // cDataS.show()
    // series.show()
    // the seires data has headers, which has to be removed, but since join isn gonna be done on sid, it will get eliminated
    // join with si a typed fucntion which can be sued in DS
    // but it does not like string as column name which has to be joined on
    // also DS does not like having same columns names even for  join
    // changeng the case class and schema options to set a different name for joining columns

    val joined1 =  cDataS.joinWith(series,  'ids === 'id )
    // joined1.show()

    // this will give empty table as the csv file read in,Cdatas does not trim the space so the space makes id look different
    val cDataST = spark.read.schema(Encoders.product[ldatas].schema).option("header",true).option("delimiter","\t")
      .csv("E:\\Concepts\\Spark\\Data_sets\\la_county.txt")
      .select(trim('ids) as "ids",'year,'period,'value)  // fucntions._
      .as[ldatas]
      .cache()

    val joined =  cDataST.joinWith(series,  'ids === 'id )
    joined.show()

    // sampling fuunction
    println(joined.count())
    val small_joined = joined.sample(false, 0.1) // withouth replacement and just 10% of the data
    small_joined.show()
    println(small_joined.count())


    // to get nicely typed values:
    println(joined.first())
    //*******************************************************************************************************
    // readin in the zip code data to join
    // but there are missing in latitude and longitude
    // case class for zip is added
    val zipData = spark.read.schema(Encoders.product[zipcode].schema).option("header", true)
      .csv("E:\\Concepts\\Spark\\Data_sets\\zip_codes_states.csv")
        .as[zipcode].filter('lat.isNotNull)  // removing missing data rows
          .cache()

    zipData.show

    // this has county wise which might have many zipcodes
    // grouping county wise and avg lati and longi
    // agg in  this needs a typed intput
    val countyj = zipData.groupByKey(zd  => zd.county -> zd.state).agg(avg('lat).as[Double], avg('longi).as[Double])
      .map{case((county,state),lat,longi) => countyzip(lat,longi,state,county)}.cache()

    countyj.show
    // in the next this will be joined to the other data

    spark.stop()
  }
}

/*
+--------------------+--------------------+
|                  _1|                  _2|
+--------------------+--------------------+
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
+--------------------+--------------------+
only showing top 20 rows

155159
18/03/27 18:21:27 WARN Executor: 1 block locks were not released by TID = 14:
[rdd_8_0]
+--------------------+--------------------+
|                  _1|                  _2|
+--------------------+--------------------+
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
+--------------------+--------------------+
only showing top 20 rows

15688
(ldatas(LAUCN010010000000003,1990,M01,6.4),Series2(LAUCN010010000000003,CN0100100000000,03,Unemployment Rate: Autauga County, AL (U)))
18/03/27 18:21:28 WARN Executor: 1 block locks were not released by TID = 23:
[rdd_8_0]
18/03/27 18:21:29 WARN Executor: 1 block locks were not released by TID = 24:
[rdd_54_0]
+-------+---------+----------+-----------+-----+-----------+
|zipCode|      lat|     longi|       city|state|     county|
+-------+---------+----------+-----------+-----+-----------+
|  00501|40.922326|-72.637078| Holtsville|   NY|    Suffolk|
|  00544|40.922326|-72.637078| Holtsville|   NY|    Suffolk|
|  00601|18.165273|-66.722583|   Adjuntas|   PR|   Adjuntas|
|  00602|18.393103|-67.180953|     Aguada|   PR|     Aguada|
|  00603|18.455913| -67.14578|  Aguadilla|   PR|  Aguadilla|
|  00604| 18.49352|-67.135883|  Aguadilla|   PR|  Aguadilla|
|  00605|18.465162|-67.141486|  Aguadilla|   PR|  Aguadilla|
|  00606|18.172947|-66.944111|    Maricao|   PR|    Maricao|
|  00610|18.288685|-67.139696|     Anasco|   PR|     Anasco|
|  00611|18.279531| -66.80217|    Angeles|   PR|     Utuado|
|  00612|18.450674|-66.698262|    Arecibo|   PR|    Arecibo|
|  00613|18.458093|-66.732732|    Arecibo|   PR|    Arecibo|
|  00614|18.429675|-66.674506|    Arecibo|   PR|    Arecibo|
|  00616|18.444792|-66.640678|   Bajadero|   PR|    Arecibo|
|  00617|18.447092|-66.544255|Barceloneta|   PR|Barceloneta|
|  00622|17.998531|-67.187318|   Boqueron|   PR|  Cabo Rojo|
|  00623|18.062201|-67.149541|  Cabo Rojo|   PR|  Cabo Rojo|
|  00624|18.023535|-66.726156|   Penuelas|   PR|   Penuelas|
|  00627|18.477891| -66.85477|      Camuy|   PR|      Camuy|
|  00631|18.269187|-66.864993|   Castaner|   PR|      Lares|
+-------+---------+----------+-----------+-----+-----------+
only showing top 20 rows

18/03/27 18:21:32 WARN Executor: 1 block locks were not released by TID = 27:
[rdd_63_1]
+------------------+------------------+-----+-----------------+
|               lat|             longi|state|           county|
+------------------+------------------+-----+-----------------+
|         18.206489|        -65.901774|   PR|           Juncos|
|30.171096833333337|       -82.9447875|   FL|         Suwannee|
| 33.94861809999999|       -90.2193918|   MS|     Tallahatchie|
|       31.20630475|-88.69918899999999|   MS|           Greene|
| 37.09108894444445|-83.40071711111112|   KY|           Leslie|
|45.028268600000004|        -96.199767|   MN|    Lac Qui Parle|
|       41.57341525|      -97.52228425|   NE|           Platte|
| 34.50243666666666|-97.04443266666668|   OK|           Murray|
|       36.35631025|        -95.608913|   OK|           Rogers|
| 29.30561584615385|-96.17365015384614|   TX|          Wharton|
|         18.200898|        -66.307236|   PR|     Barranquitas|
| 44.86184144444444|-72.90003944444443|   VT|         Franklin|
|         37.310451|        -76.746769|   VA|Williamsburg City|
|36.952666444444446|-82.63049277777777|   VA|             Wise|
|       31.20813875|        -81.472808|   GA|            Glynn|
|        29.9320066|-83.54808399999999|   FL|           Taylor|
|28.241715723404255|-80.68458823404255|   FL|          Brevard|
|        34.0450096|       -89.7152134|   MS|        Yalobusha|
|         32.699154|        -89.114395|   MS|          Neshoba|
| 37.14806633333333|-83.74170944444445|   KY|             Clay|
+------------------+------------------+-----+-----------------+
only showing top 20 rows


 */