/* after installing spark on the local machine
in the terminal type in spark-shell --master local[*]
-- master signifies this machine is the main one
local[*] will allocate all the cores on the system
Spark repl is based on scala
sc = spark context, gives access to what spark can do
> textfile can be opened: sc.textFile("<___>.<_>")
spark = spark session
cluster is to harness the full power fo distributed system
to create them we need sbt-assembly -> to export fat jars -> has libraries which can be run on a server using spark-submit
> spark-submit --master local[*] --class 'jarFile' in target/scala/...
add sbt assembly dependencies to assembly.sbt like build.sbt
*/