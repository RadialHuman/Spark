import org.apache.spark.{SparkConf, SparkContext}
// 20. PairRDDFunctions Part 1 in Spark (using Scala)
// spl functions for rdds that are in key value pair
// keys can be replicated with different values
// this is fucndametal to big data data

case class TempData9(day:Int, jd:Int, month:Int, year:Int, precip:Double, snow:Double, avgT:Int, maxT:Int, minT:Int)


object rdd6 {

  def toDoubleOrNeg(s: String): Double = {
    try {
      s.toDouble
    }
    catch {
      case _: NumberFormatException => -1
    }
  }


  def main(s: Array[String]): Unit = {

    val conf = new SparkConf().setAppName("rdd1")
      .setMaster("local[*]")
    val sc = new SparkContext(conf)


    val data = sc.textFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv").filter(!_.contains("Day "))

    val rows = data.flatMap { l =>
      val p = l.split(",")
      if (p(6) == "." || p(7) == "." || p(8) == ".") Seq.empty
      else
        Seq(TempDataCC(p(0).toInt, p(1).toInt, p(2).toInt, p(3).toInt, rdd6.toDoubleOrNeg(p(4)), rdd6.toDoubleOrNeg(p(5)), p(6).toInt, p(7).toInt, p(8).toInt))
    }.cache()

    // fold reduce and aggregate based on keys
    // cogroup, for grouping multiple rdds, to get keys with botht the values, each key will appear once
    // goupwith is similar to cogroup
    // joins, like sql outer left right full, keys can appear multiple times -- b careful as usual
    // subtractbykey is handy not sure

    // avg temp by year using aggregatebykey
    // rows is an rdd of tempdata
    val keyByYear = rows.map(td => td.year -> td) // this is now a rdd of tuples i.e. paried rdd
    val avgTemp = keyByYear.aggregateByKey(0.0 -> 0)
                  ({case ((sum,count),td) => (sum + td.maxT, count+1) },
                   {case ((sum1,count1),(sum2,count2))=> (sum1+sum2, count1+count2)})
    avgTemp foreach println




  }
}