//7. Data Processing with Higher-order Methods Part 6 (Scala)
// Higher order methods to do some analysis in spark
// finding average temp by month

case class TempDataCC7(day:Int, jd:Int, month:Int, year:Int, precip:Double, snow:Double, avgT:Int, maxT:Int, minT:Int)

object TempData7 { // for precipitation and snow fall to convert "." to -1
  def toDoubleOrNeg(s:String):Double={
    try{
      s.toDouble
    }
    catch{
      case  _:NumberFormatException => -1
    }
  }

  def main(s:Array[String]):Unit={
    val source = scala.io.Source.fromFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv")
    val lines = source.getLines()
    //println("The number of rows before cleaning %d ", lines.length)
    val data = lines.drop(1)
    val rows  = data.flatMap{ l=> // this is where this one differs from the previous one, flatmap would take in a seq
      val p = l.split(",")
      if(p(6)=="."||p(7)=="."||p(8)==".") Seq.empty
      else
        Seq(TempDataCC(p(0).toInt,p(1).toInt,p(2).toInt,p(3).toInt,toDoubleOrNeg(p(4)),toDoubleOrNeg(p(5)),p(6).toInt,p(7).toInt,p(8).toInt))
    }.toArray
    source.close()

    // loop can be used to store the temp month wise and find an average but
    // many passes and same work is done 12 times so inefficient
    // thus groupby

    val months = rows.groupBy(_.month)
    val avgtemp = months.map{case (m, days)=> m -> days.foldLeft(0.0)((sum, td) => sum + td.maxT)/days.length}
    println(avgtemp) // does not present in a nice manner, but the map is in a format thats not controllable
    avgtemp.toSeq.sortBy(_._1) foreach println //this is a small seq and so the creation of new will not affect efficiency

  }
}

/*
Map(5 -> 66.64433135425502, 10 -> 55.87532097004279, 1 -> 16.16281112737921, 6 -> 75.19371727748691, 9 -> 68.98621877691644, 2 -> 21.044573643410853, 12 -> 21.44126804415511, 7 -> 80.85272625070264, 3 -> 34.81650848432557, 11 -> 36.33536029188203, 8 -> 78.97310170916224, 4 -> 52.93286010056197)
(1,16.16281112737921)
(2,21.044573643410853)
(3,34.81650848432557)
(4,52.93286010056197)
(5,66.64433135425502)
(6,75.19371727748691)
(7,80.85272625070264)
(8,78.97310170916224)
(9,68.98621877691644)
(10,55.87532097004279)
(11,36.33536029188203)
(12,21.44126804415511)
 */

