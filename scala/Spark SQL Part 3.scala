import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StructField, StructType, DoubleType, StringType, DateType}
// 30. Spark SQL Part 3 (using Scala)

object sql5 {
  def main(s: Array[String]): Unit = {

    //  val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]") not anymore
    val spark = SparkSession.builder().master("local[*]").appName("ssql").getOrCreate()
    import spark.implicits._ // useful stuff

    spark.sparkContext.setLogLevel("WARN") // less verbose

    // since the schema is all messed up, we need to create sschemas for all the columns we need
    // StructType defines the rows and thier types in the dataframe
    val tsch = StructType(Array(
      StructField("sId",StringType),
      StructField("date",DateType), // the date format is not the standard in the input, intimate the user
      StructField("measureType",StringType),
      StructField("value",DoubleType)
    ) )
    // reading command changes here to define the schema
    // option is used to represent the format in which the input field is
    val data17 = spark.read.schema(tsch).option("dateFormat","yyyyMMdd").csv("E:\\Concepts\\Spark\\Data_sets\\2017.csv")
    data17.show()
    data17.schema.printTreeString()



    spark.stop()
  }
}

/*

+-----------+----------+-----------+-----+
|        sId|      date|measureType|value|
+-----------+----------+-----------+-----+
|US1MNHN0184|2017-01-01|       PRCP|  0.0|
|US1MNHN0184|2017-01-01|       SNOW|  0.0|
|US1MNHN0184|2017-01-01|       SNWD|274.0|
|US1MNWR0029|2017-01-01|       PRCP|  0.0|
|CA1MB000296|2017-01-01|       PRCP|  0.0|
|ASN00015643|2017-01-01|       TMAX|274.0|
|ASN00015643|2017-01-01|       TMIN|218.0|
|ASN00015643|2017-01-01|       PRCP|  2.0|
|US1MNCV0008|2017-01-01|       PRCP|  0.0|
|US1MNCV0008|2017-01-01|       SNOW|  0.0|
|US1MISW0005|2017-01-01|       PRCP|  0.0|
|US1MISW0005|2017-01-01|       SNOW|  0.0|
|US1MISW0005|2017-01-01|       SNWD|  0.0|
|ASN00085296|2017-01-01|       TMAX|217.0|
|ASN00085296|2017-01-01|       TMIN|127.0|
|ASN00085296|2017-01-01|       PRCP|  0.0|
|ASN00085280|2017-01-01|       TMAX|215.0|
|ASN00085280|2017-01-01|       TMIN|156.0|
|ASN00085280|2017-01-01|       PRCP|  0.0|
|US1MAMD0069|2017-01-01|       PRCP| 56.0|
+-----------+----------+-----------+-----+
only showing top 20 rows

root
 |-- sId: string (nullable = true)
 |-- date: date (nullable = true)
 |-- measureType: string (nullable = true)
 |-- value: double (nullable = true)

 */