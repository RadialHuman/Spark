import org.apache.spark.SparkConf
import org.apache.spark.sql.{Row, SparkSession, Encoders}
import org.apache.spark.sql.types.{DateType, DoubleType, StringType, StructField, StructType}
import org.apache.spark.sql.functions._


// 42 and 43. Spark SQL- Typed Datasets Part 4 and 5(using Scala)


// case class Series2 (id: String, area: String, measure:String, title: String)
 case class ldatas (ids: String, year: Int, period: String, value:Double)

// to read the series data, either the series case class has to be updated or like in rdd we can read in the lines as txt
// and map it manually

object ds3 {
  def main(s: Array[String]): Unit = {


    val spark = SparkSession.builder().master("local[*]").getOrCreate()
    import spark.implicits._

    spark.sparkContext.setLogLevel("WARN")

    // option one, read and give a schema with options
    val cDataS = spark.read.schema(Encoders.product[ldatas].schema).option("header",true).option("delimiter","\t")
                .csv("E:\\Concepts\\Spark\\Data_sets\\la_county.txt")
                .as[ldatas]
                .cache()

    // option two, read in as a textfile, get dataset of strings, then convert to dataset of series case class
    val series = spark.read.textFile("E:\\Concepts\\Spark\\Data_sets\\la_series.txt").map{ line =>
      val p = line.split("\t").map(_.trim)
      Series2(p(0),p(2),p(3),p(6))
    }.cache()



    // cDataS.show()
    // series.show()
    //*******************************************************************************************************
    // the seires data has headers, which has to be removed, but since join isn gonna be done on sid, it will get eliminated
    // join with si a typed fucntion which can be sued in DS
    // but it does not like string as column name which has to be joined on
    // also DS does not like having same columns names even for  join
    // changeng the case class and schema options to set a different name for joining columns
    //*******************************************************************************************************

    val joined1 =  cDataS.joinWith(series,  'ids === 'id )
    // joined1.show()

    // this will give empty table as the csv file read in,Cdatas does not trim the space so the space makes id look different
    val cDataST = spark.read.schema(Encoders.product[ldatas].schema).option("header",true).option("delimiter","\t")
      .csv("E:\\Concepts\\Spark\\Data_sets\\la_county.txt")
      .select(trim('ids) as "ids",'year,'period,'value)  // fucntions._
      .as[ldatas]
      .cache()

    val joined =  cDataST.joinWith(series,  'ids === 'id )
    joined.show()

    // sampling fuunction
    println(joined.count())
    val small_joined = joined.sample(false, 0.1) // withouth replacement and just 10% of the data
    small_joined.show()
    println(small_joined.count())


    // to get nicely typed values:
    println(joined.first())

    spark.stop()
  }
}

/*
+--------------------+--------------------+
|                  _1|                  _2|
+--------------------+--------------------+
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
|[LAUCN02158000000...|[LAUCN02158000000...|
+--------------------+--------------------+
only showing top 20 rows

155159
18/03/25 12:25:23 WARN Executor: 1 block locks were not released by TID = 14:
[rdd_8_0]
+--------------------+--------------------+
|                  _1|                  _2|
+--------------------+--------------------+
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
|[LAUCN01001000000...|[LAUCN01001000000...|
+--------------------+--------------------+
only showing top 20 rows

15345

(ldatas(LAUCN010010000000003,1990,M01,6.4),Series2(LAUCN010010000000003,CN0100100000000,03,Unemployment Rate: Autauga County, AL (U)))

 */