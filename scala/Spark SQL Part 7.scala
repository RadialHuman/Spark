import org.apache.spark.SparkConf
import org.apache.spark.sql.{Row, SparkSession}
import org.apache.spark.sql.types.{DateType, DoubleType, StringType, StructField, StructType}
// 34. Spark SQL Part 7  (using Scala)

object sql9 {
  def main(s: Array[String]): Unit = {

    //  val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]") not anymore
    val spark = SparkSession.builder().master("local[*]").appName("ssql").getOrCreate()
    import spark.implicits._ // useful stuff

    spark.sparkContext.setLogLevel("WARN") // less verbose

    // since the schema is all messed up, we need to create sschemas for all the columns we need
    // StructType defines the rows and thier types in the dataframe
    val tsch = StructType(Array(
      StructField("sId",StringType),
      StructField("date",DateType), // the date format is not the standard in the input, intimate the user
      StructField("measureType",StringType),
      StructField("value",DoubleType)
    ) )
    // reading command changes here to define the schema
    // option is used to represent the format in which the input field is
    val data17 = spark.read.schema(tsch).option("dateFormat","yyyyMMdd").csv("E:\\Concepts\\Spark\\Data_sets\\2017.csv")

   // val tmax17 = data17.filter($"measureType"==="TMAX")
    // val tmin17 = data17.filter('measureType==="TMIN")  // this si smilar but uses scala symbol to column


    val tmax17 = data17.filter($"measureType"==="TMAX").limit(1000000).drop("measureType").withColumnRenamed("value","TMAX")
    val tmin17 = data17.filter('measureType==="TMIN").limit(1000000).drop("measureType").withColumnRenamed("value","TMIN")
    //val combo17 = tmax17.join(tmin17, tmax17("sId") === tmin17("sId") && tmax17("date") === tmin17("date"))
    // combo17.show()


    val combo17 = tmax17.join(tmin17, Seq("sId","date")) // by default is an inner join, so uncommon columns/rows will be thrown away

    // for average between two columns
    val avg17 = combo17.select('sId,'date,('TMAX+'TMIN)/2)


   // goal is to show avg at global level
    // reading another file for latitude and longitude info, which is a text file in a bad format, space and non standard stuff
    // read txt into rdd, amp torows and convert into dataframe with user defiend schema
    val new_schema = StructType(Array(
      StructField("sId",StringType),
      StructField("lat",DoubleType),
      StructField("lon",DoubleType),
      StructField("name",StringType)

    ))
    // gives this in a string format so mapping
    val sRDD = spark.sparkContext.textFile("E:\\Concepts\\Spark\\Data_sets\\ghcnd-stations.txt").map{ l =>
      val id = l.substring(0,11)
      val lat = l.substring(12,20).toDouble
      val lon = l.substring(21,30).toDouble
      val name  = l.substring(41,71)
      Row(id,lat,lon,name) // many rows there so that can be a problem
    }

    // convertign it to DF
    val stations = spark.createDataFrame(sRDD,new_schema).cache()
    stations.show()


    spark.stop()
  }
}


/*

+-----------+-------+--------+--------------------+
|        sId|    lat|     lon|                name|
+-----------+-------+--------+--------------------+
|ACW00011604|17.1167|-61.7833|ST JOHNS COOLIDGE...|
|ACW00011647|17.1333|-61.7833|ST JOHNS         ...|
|AE000041196| 25.333|  55.517|SHARJAH INTER. AI...|
|AEM00041194| 25.255|  55.364|DUBAI INTL       ...|
|AEM00041217| 24.433|  54.651|ABU DHABI INTL   ...|
|AEM00041218| 24.262|  55.609|AL AIN INTL      ...|
|AF000040930| 35.317|  69.017|NORTH-SALANG     ...|
|AFM00040938|  34.21|  62.228|HERAT            ...|
|AFM00040948| 34.566|  69.212|KABUL INTL       ...|
|AFM00040990|   31.5|   65.85|KANDAHAR AIRPORT ...|
|AG000060390|36.7167|    3.25|ALGER-DAR EL BEID...|
|AG000060590|30.5667|  2.8667|EL-GOLEA         ...|
|AG000060611|  28.05|  9.6331|IN-AMENAS        ...|
|AG000060680|   22.8|  5.4331|TAMANRASSET      ...|
|AGE00135039|35.7297|    0.65|ORAN-HOPITAL MILI...|
|AGE00147704|  36.97|    7.79|ANNABA-CAP DE GAR...|
|AGE00147705|  36.78|    3.07|ALGIERS-VILLE/UNI...|
|AGE00147706|   36.8|    3.03|ALGIERS-BOUZAREAH...|
|AGE00147707|   36.8|    3.04|ALGIERS-CAP CAXIN...|
|AGE00147708|  36.72|    4.05|TIZI OUZOU       ...|
+-----------+-------+--------+--------------------+
only showing top 20 rows
 */