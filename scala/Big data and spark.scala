// 12. Big Data and Spark


/* if the dataset or processing of dataset consumes more memory than the memory of your machine
thus the distribution of processing and data has to happen to work with it
instead of buying bigger and powerful machines
multiple machines with 32 to 64 gb ram can be used to setup the infra

It all started with MapReduce in Google in 2004, to deal with datasets
> an avg life time of a comp is 3-4 years
> but when MapReduce had clusters with 1000s of machines, the machine could fail 1/day
> recovery is imp now, MR saves the processing's output repeatedly
> Hadoop used MR, then grew into a ecosystem
> Challenge was: latency as the data was in the disk and in the network
> This slows down due to disk operation

Spark was born in Berkley then moved to Apache
> It runs on memory and not disks
> Though there si the network  latency, it does not save vary often like hadoop
> RDD - Resilient distributed datasets is the basis , this becomes dataframes and then datasets (in progress)
> Dealing with 1 machine dying /day is dealt in a different way in Spark compared to hadoop
> ...
*/