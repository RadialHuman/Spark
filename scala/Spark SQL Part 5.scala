import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StructField, StructType, DoubleType, StringType, DateType}
// 32. Spark SQL Part 5 (using Scala)

object sql7 {
  def main(s: Array[String]): Unit = {

    //  val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]") not anymore
    val spark = SparkSession.builder().master("local[*]").appName("ssql").getOrCreate()
    import spark.implicits._ // useful stuff

    spark.sparkContext.setLogLevel("WARN") // less verbose

    // since the schema is all messed up, we need to create sschemas for all the columns we need
    // StructType defines the rows and thier types in the dataframe
    val tsch = StructType(Array(
      StructField("sId",StringType),
      StructField("date",DateType), // the date format is not the standard in the input, intimate the user
      StructField("measureType",StringType),
      StructField("value",DoubleType)
    ) )
    // reading command changes here to define the schema
    // option is used to represent the format in which the input field is
    val data17 = spark.read.schema(tsch).option("dateFormat","yyyyMMdd").csv("E:\\Concepts\\Spark\\Data_sets\\2017.csv")

   // val tmax17 = data17.filter($"measureType"==="TMAX")
    // val tmin17 = data17.filter('measureType==="TMIN")  // this si smilar but uses scala symbol to column

    // next video would be in performing operatiosn on tables
    // get avg temp for tmax and tmin, for same station id and date, group by them and avg it
    // but first join the max and min dfs
    // there many ways to join just like to filter few in experimental stage for datasets and some in old scala ways
    // this will not work as tis is a huge file and the system is standalone
    // so using sql's limit we can shrink the size of the datafrmes
    //  and to make the output more clear, drop measure type column and rename the values with tmax and tmin

    val tmax17 = data17.filter($"measureType"==="TMAX").limit(1000).drop("measureType").withColumnRenamed("value","TMAX")
    val tmin17 = data17.filter('measureType==="TMIN").limit(1000).drop("measureType").withColumnRenamed("value","TMIN")
    //val combo17 = tmax17.join(tmin17, tmax17("sId") === tmin17("sId") && tmax17("date") === tmin17("date"))
    // combo17.show()

    // this does not give a clear result as the column names are the same and values are repeated
    // many ways and many things to fix tis

    val combo17 = tmax17.join(tmin17, Seq("sId","date")) // by default is an inner join, so uncommon columns/rows will be thrown away
    combo17.show()

    // for average between two columns
    val avg17 = combo17.select('sId,'date,('TMAX+'TMIN)/2)
    avg17.show()

    // in the next one, other file will be operated and them combined to show global data

    spark.stop()
  }
}

/*
+-----------+----------+-----------+-----+-----------+----------+-----------+------+
|        sId|      date|measureType|value|        sId|      date|measureType| value|
+-----------+----------+-----------+-----+-----------+----------+-----------+------+
|ASN00015643|2017-01-01|       TMAX|274.0|ASN00015643|2017-01-01|       TMIN| 218.0|
|ASN00085296|2017-01-01|       TMAX|217.0|ASN00085296|2017-01-01|       TMIN| 127.0|
|ASN00085280|2017-01-01|       TMAX|215.0|ASN00085280|2017-01-01|       TMIN| 156.0|
|ASN00040209|2017-01-01|       TMAX|293.0|ASN00040209|2017-01-01|       TMIN| 250.0|
|ASN00068151|2017-01-01|       TMAX|254.0|ASN00068151|2017-01-01|       TMIN| 200.0|
|USW00024061|2017-01-01|       TMAX|-77.0|USW00024061|2017-01-01|       TMIN|-205.0|
|USW00024229|2017-01-01|       TMAX| 44.0|USW00024229|2017-01-01|       TMIN|   0.0|
|USW00094626|2017-01-01|       TMAX| 11.0|USW00094626|2017-01-01|       TMIN| -88.0|
|USW00012876|2017-01-01|       TMAX|278.0|USW00012876|2017-01-01|       TMIN| 128.0|
|USW00014719|2017-01-01|       TMAX| 89.0|USW00014719|2017-01-01|       TMIN| -66.0|
|USW00004131|2017-01-01|       TMAX|-85.0|USW00004131|2017-01-01|       TMIN|-265.0|
|USW00012842|2017-01-01|       TMAX|272.0|USW00012842|2017-01-01|       TMIN| 150.0|
|USW00003967|2017-01-01|       TMAX| 72.0|USW00003967|2017-01-01|       TMIN| -55.0|
|USS0018F01S|2017-01-01|       TMAX|-34.0|USS0018F01S|2017-01-01|       TMIN|-115.0|
|USS0019L03S|2017-01-01|       TMAX| 41.0|USS0019L03S|2017-01-01|       TMIN|-102.0|
|USW00003048|2017-01-01|       TMAX|105.0|USW00003048|2017-01-01|       TMIN|   6.0|
|USW00003889|2017-01-01|       TMAX|100.0|USW00003889|2017-01-01|       TMIN|  39.0|
|USS0014G01S|2017-01-01|       TMAX|-42.0|USS0014G01S|2017-01-01|       TMIN|-107.0|
|USS0017B04S|2017-01-01|       TMAX|-62.0|USS0017B04S|2017-01-01|       TMIN|-128.0|
|USS0014C04S|2017-01-01|       TMAX|-68.0|USS0014C04S|2017-01-01|       TMIN|-185.0|
+-----------+----------+-----------+-----+-----------+----------+-----------+------+
only showing top 20 rows

+-----------+----------+-----+------+
|        sId|      date| TMAX|  TMIN|
+-----------+----------+-----+------+
|ASN00015643|2017-01-01|274.0| 218.0|
|ASN00085296|2017-01-01|217.0| 127.0|
|ASN00085280|2017-01-01|215.0| 156.0|
|ASN00040209|2017-01-01|293.0| 250.0|
|ASN00068151|2017-01-01|254.0| 200.0|
|USW00024061|2017-01-01|-77.0|-205.0|
|USW00024229|2017-01-01| 44.0|   0.0|
|USW00094626|2017-01-01| 11.0| -88.0|
|USW00012876|2017-01-01|278.0| 128.0|
|USW00014719|2017-01-01| 89.0| -66.0|
|USW00004131|2017-01-01|-85.0|-265.0|
|USW00012842|2017-01-01|272.0| 150.0|
|USW00003967|2017-01-01| 72.0| -55.0|
|USS0018F01S|2017-01-01|-34.0|-115.0|
|USS0019L03S|2017-01-01| 41.0|-102.0|
|USW00003048|2017-01-01|105.0|   6.0|
|USW00003889|2017-01-01|100.0|  39.0|
|USS0014G01S|2017-01-01|-42.0|-107.0|
|USS0017B04S|2017-01-01|-62.0|-128.0|
|USS0014C04S|2017-01-01|-68.0|-185.0|
+-----------+----------+-----+------+
only showing top 20 rows

+-----------+----------+-------------------+
|        sId|      date|((TMAX + TMIN) / 2)|
+-----------+----------+-------------------+
|ASN00015643|2017-01-01|              246.0|
|ASN00085296|2017-01-01|              172.0|
|ASN00085280|2017-01-01|              185.5|
|ASN00040209|2017-01-01|              271.5|
|ASN00068151|2017-01-01|              227.0|
|USW00024061|2017-01-01|             -141.0|
|USW00024229|2017-01-01|               22.0|
|USW00094626|2017-01-01|              -38.5|
|USW00012876|2017-01-01|              203.0|
|USW00014719|2017-01-01|               11.5|
|USW00004131|2017-01-01|             -175.0|
|USW00012842|2017-01-01|              211.0|
|USW00003967|2017-01-01|                8.5|
|USS0018F01S|2017-01-01|              -74.5|
|USS0019L03S|2017-01-01|              -30.5|
|USW00003048|2017-01-01|               55.5|
|USW00003889|2017-01-01|               69.5|
|USS0014G01S|2017-01-01|              -74.5|
|USS0017B04S|2017-01-01|              -95.0|
|USS0014C04S|2017-01-01|             -126.5|
+-----------+----------+-------------------+
only showing top 20 rows


 */