import org.apache.spark.{SparkConf, SparkContext}
// 17. Spark RDDs Part 4 (using Scala)
// replicating all the operatiosn done in 6 parts of Data processing with higher order fucntions using Spark


object rdd4 {
  def main(s: Array[String]): Unit = {

    val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]")
    val sc = new SparkContext(conf)

    val data = sc.textFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv").filter(!_.contains("Day "))
    val rows = data.flatMap { l =>
      val p = l.split(",")
      if (p(6) == "." || p(7) == "." || p(8) == ".") Seq.empty
      else Seq(TempDataCC(p(0).toInt, p(1).toInt, p(2).toInt, p(3).toInt, TempData5.toDoubleOrNeg(p(4)), TempData5.toDoubleOrNeg(p(5)), p(6).toInt, p(7).toInt, p(8).toInt))

    }.cache()
    // par in scala is not required spark
    // in spark everything is an rdd nothing like map or seq
    // and iterable is not ok with length, so size
    // foreach doesnot work in order so it might give sorted data in a shuffled way
    // so use collect with foreach
  }
}