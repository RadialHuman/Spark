import org.apache.spark.sql.{Encoders, SparkSession}

import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.feature.VectorAssembler
// 50. Spark ML Linear Regression Part 1 (using Scala)

//Regression is when the value is continuos
// classification is bucketing

//Linear regression:

case class noa2017(sId: String, date: java.sql.Date, measure: String, value: Double) // rest is ignored
case class Station(sid:String, lat:Double, lon: Double, ele:Double, name:String)


object lr1  {
  def main(s: Array[String]): Unit = {

    val spark = SparkSession.builder().master("local[*]").getOrCreate()
    import spark.implicits._

    spark.sparkContext.setLogLevel("WARN")
    val stats = spark.read.textFile("E:\\Concepts\\Spark\\Data_sets\\ghcnd-stations.txt").map{
      line =>
        val id = line.substring(0,11)
        val lat =  line.substring(12,20).trim.toDouble
        val lon = line.substring(21,30).trim.toDouble
        val ele =  line.substring(31,37).trim.toDouble
        val name =  line.substring(41,71)
        Station(id, lat, lon, ele, name)

    }.cache()

    val dataFroModel = new VectorAssembler().setInputCols(Array("lat","lon")).setOutputCol("location")
    val statsWithLoc = dataFroModel.transform(stats)
    // statsWithLoc.show()


    val kMeans = new KMeans().setK(2000).setFeaturesCol("location").setPredictionCol("cluster")
    val statsClusterModel = kMeans.fit(statsWithLoc)
    val clusterdOuput = statsClusterModel.transform(statsWithLoc)
    // clusterdOuput.show()

    //**********************************************************************************************

    val data17 = spark.read.schema(Encoders.product[noa2017].schema)
        .option(" dateFormat","yyyyDDmm")  // for date format yyyyyddmm
        .csv("E:\\Concepts\\Spark\\Data_sets\\2017.csv")
    data17.show()

    // need for regression here is to
    val cs = clusterdOuput.filter('cluster === 441).select('sId)
    val cd = data17.filter('measure === "TMAX").join(cs,"sId")

    cd.show()
    spark.stop()

  }
}



/*


 */