//2. Data Processing with Higher-order Methods Part 1 (Scala)
case class TempDataCC(day:Int, jd:Int, month:Int, year:Int, precip:Double, snow:Double, avgT:Int, maxT:Int, minT:Int)

object TempData {
  def main(s:Array[String]):Unit={
    val source = scala.io.Source.fromFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv")
    val lines = source.getLines()
    val data = lines.drop(1) // remove header
    //for(i <- data) println(i)
    // case class for the rows
    val rows  = data.map{ l=>
      val p = l.split(",")
      TempDataCC(p(0).toInt,p(1).toInt,p(2).toInt,p(3).toInt,p(4).toDouble,p(5).toDouble,p(6).toInt,p(7).toInt,p(8).toInt)
      // this will give an iterator, so it will be consumed only once, to prevent that make it an Array
    }.toArray
    source.close()

    rows.take(5) foreach println
  }
}

// moral of the story clean and check the data before processing it.
// the dots are missing data and cannot be converted
// if the missing values where filled, the output would have been
/*
TempDataCC(1,335,12,1895,0.0,0.0,12,26,-2)
TempDataCC(2,336,12,1895,0.0,0.0,-3,11,-16)
TempDataCC(3,337,12,1895,0.0,0.0,6,11,0)
TempDataCC(4,338,12,1895,0.0,0.0,2,12,-9)
TempDataCC(5,339,12,1895,0.0,0.0,19,26,11)

 */
