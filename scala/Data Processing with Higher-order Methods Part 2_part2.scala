//2. Data Processing with Higher-order Methods Part 2 (Scala)
// from the previous one, missing data is a problem. To solve it
// 1. lines are to be read line/line and dots should be processed or <- focus
// 2. lines with these dots can be eliminated
//
// In this case we would substitute "." with a number which cant happen
case class TempDataCC3(day:Int, jd:Int, month:Int, year:Int, precip:Double, snow:Double, avgT:Int, maxT:Int, minT:Int)

object TempData3 { // for precipitation and snow fall to convert "." to -1
  def toDoubleOrNeg(s:String):Double={
    try{
      s.toDouble
    }
    catch{
      case  _:NumberFormatException => -1
    }
  }

  def main(s:Array[String]):Unit={
    val source = scala.io.Source.fromFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv")
    val lines = source.getLines()
    //println("The number of rows before cleaning %d ", lines.length)
    val data = lines.drop(1)
    val rows  = data.flatMap{ l=> // this is where this one differs from the previous one, flatmap would take in a seq
      val p = l.split(",")
      if(p(6)=="."||p(7)=="."||p(8)==".") Seq.empty
      else
        Seq(TempDataCC(p(0).toInt,p(1).toInt,p(2).toInt,p(3).toInt,toDoubleOrNeg(p(4)),toDoubleOrNeg(p(5)),p(6).toInt,p(7).toInt,p(8).toInt))
    }.toArray
    source.close()
    println("The number of rows after cleaning %d ", rows.length)
    rows.take(45,50) foreach println

  }
}

/*
(The number of rows after cleaning %d ,41281)
TempDataCC(1,335,12,1895,0.0,0.0,12,26,-2)
TempDataCC(2,336,12,1895,0.0,0.0,-3,11,-16)
TempDataCC(3,337,12,1895,0.0,0.0,6,11,0)
TempDataCC(4,338,12,1895,0.0,0.0,2,12,-9)
TempDataCC(5,339,12,1895,0.0,0.0,19,26,11)
...
TempDataCC(17,17,1,1896,0.0,-1.0,-3,11,-16)
TempDataCC(18,18,1,1896,0.0,-1.0,-7,6,-19)
TempDataCC(19,19,1,1896,0.0,-1.0,4,21,-14)
TempDataCC(20,20,1,1896,0.0,-1.0,19,33,4)
TempDataCC(21,21,1,1896,0.0,-1.0,23,36,9)
 */

