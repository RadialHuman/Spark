import org.apache.spark.{SparkConf, SparkContext}
// 15. Spark RDDs Part 2 (using Scala)
// replicating all the operatiosn done in 6 parts of Data processing with higher order fucntions using Spark


object rdd2 {
  def main(s: Array[String]): Unit = {

    val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]")
    val sc = new SparkContext(conf)

    val data = sc.textFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv").filter(!_.contains("Day "))
    val rows = data.flatMap { l =>
      val p = l.split(",")
      if (p(6) == "." || p(7) == "." || p(8) == ".") Seq.empty
      else
        Seq(TempDataCC(p(0).toInt, p(1).toInt, p(2).toInt, p(3).toInt, TempData5.toDoubleOrNeg(p(4)), TempData5.toDoubleOrNeg(p(5)), p(6).toInt, p(7).toInt, p(8).toInt))

      // to find the max temp in spark, there is no maxBy, but there is max
      // that takes an order, we can pass in an order
    }
    println(rows.max()(Ordering.by(_.maxT))) // this is an action

    // similar thing can be done by a reduce
    println(rows.reduce((td1,td2)=> if(td1.maxT >= td2.maxT) td1 else td2))

    // this code is giving rise to something not good, corrected in the next file
  }
}

/*
TempDataCC(7,189,7,1936,0.0,0.0,89,107,70)
TempDataCC(7,189,7,1936,0.0,0.0,89,107,70)
 */