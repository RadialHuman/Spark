import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{StructField, StructType, DoubleType, StringType, DateType}
// 33. Spark SQL Part 6 (using Scala)

object sql8 {
  def main(s: Array[String]): Unit = {

    //  val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]") not anymore
    val spark = SparkSession.builder().master("local[*]").appName("ssql").getOrCreate()
    import spark.implicits._ // useful stuff

    spark.sparkContext.setLogLevel("WARN") // less verbose

    // since the schema is all messed up, we need to create sschemas for all the columns we need
    // StructType defines the rows and thier types in the dataframe
    val tsch = StructType(Array(
      StructField("sId",StringType),
      StructField("date",DateType), // the date format is not the standard in the input, intimate the user
      StructField("measureType",StringType),
      StructField("value",DoubleType)
    ) )
    // reading command changes here to define the schema
    // option is used to represent the format in which the input field is
    val data17 = spark.read.schema(tsch).option("dateFormat","yyyyMMdd").csv("E:\\Concepts\\Spark\\Data_sets\\2017.csv")

   // val tmax17 = data17.filter($"measureType"==="TMAX")
    // val tmin17 = data17.filter('measureType==="TMIN")  // this si smilar but uses scala symbol to column

    // next video would be in performing operatiosn on tables
    // get avg temp for tmax and tmin, for same station id and date, group by them and avg it
    // but first join the max and min dfs
    // there many ways to join just like to filter few in experimental stage for datasets and some in old scala ways
    // this will not work as tis is a huge file and the system is standalone
    // so using sql's limit we can shrink the size of the datafrmes
    //  and to make the output more clear, drop measure type column and rename the values with tmax and tmin

    val tmax17 = data17.filter($"measureType"==="TMAX").limit(1000000).drop("measureType").withColumnRenamed("value","TMAX")
    val tmin17 = data17.filter('measureType==="TMIN").limit(1000000).drop("measureType").withColumnRenamed("value","TMIN")
    //val combo17 = tmax17.join(tmin17, tmax17("sId") === tmin17("sId") && tmax17("date") === tmin17("date"))
    // combo17.show()

    // this does not give a clear result as the column names are the same and values are repeated
    // many ways and many things to fix tis

    val combo17 = tmax17.join(tmin17, Seq("sId","date")) // by default is an inner join, so uncommon columns/rows will be thrown away
   // combo17.show()

    // for average between two columns
    val avg17 = combo17.select('sId,'date,('TMAX+'TMIN)/2)
    //avg17.show()

    // in this one first stastical values of the table
    // descriptive stats, describe does give median and quartile
    tmax17.describe().show()
    tmin17.describe().show()

    // to get that, use stat, there are oher things like approx quartile, corr, covariance, cross-tabulating
    //tmax17.stat().show()

    // aggregation can also be done , but later from functions package

    spark.stop()
  }
}
