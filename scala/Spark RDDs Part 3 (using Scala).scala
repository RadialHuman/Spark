import org.apache.spark.{SparkConf, SparkContext}
// 16. Spark RDDs Part 3 (using Scala)
// replicating all the operatiosn done in 6 parts of Data processing with higher order fucntions using Spark


object rdd3 {
  def main(s: Array[String]): Unit = {

    val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]")
    val sc = new SparkContext(conf)

    val data = sc.textFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv").filter(!_.contains("Day "))
    val rows = data.flatMap { l =>
      val p = l.split(",")
      if (p(6) == "." || p(7) == "." || p(8) == ".") Seq.empty
      else Seq(TempDataCC(p(0).toInt, p(1).toInt, p(2).toInt, p(3).toInt, TempData5.toDoubleOrNeg(p(4)), TempData5.toDoubleOrNeg(p(5)), p(6).toInt, p(7).toInt, p(8).toInt))

      // to find the max temp in spark, there is no maxBy, but there is max
      // that takes an order, we can pass in an order
    }.cache()
      // in the previous one, there was an inefficient thign goign on
      // when an action is called , the work of building an RDD is repeated , everytime row is being typed and used, reparsing of the file happens
      // when there is a rdd which is used alot, the rdd can be kept in cache (inside memory) or persist which can also use a storage level arg, and can be gudied on how to persist

      // to count rainy days and get its avg temp using count, which is liek size or lenght
      // in scala its ineffiecent to run a filter follwoed by the length or size of rdd
      // becasue the filter will build a new colelction as it is eager
      // but in Spark the filter is lazy, thus count only makes filter do whats needed
      println(rows.count())
      val raincount =rows.filter(_.precip >= 1.0).count() // this is a smart count as it knows we are interested only in the count
      println(raincount)


  }
}

/*
41281
575
 */