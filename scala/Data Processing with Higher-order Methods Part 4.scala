//4. Data Processing with Higher-order Methods Part 4 (Scala)
// Higher order methods to do some analysis
// find number of days that have more precipitation than 1

case class TempDataCC5(day:Int, jd:Int, month:Int, year:Int, precip:Double, snow:Double, avgT:Int, maxT:Int, minT:Int)

object TempData5 { // for precipitation and snow fall to convert "." to -1
  def toDoubleOrNeg(s:String):Double={
    try{
      s.toDouble
    }
    catch{
      case  _:NumberFormatException => -1
    }
  }

  def main(s:Array[String]):Unit={
    val source = scala.io.Source.fromFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv")
    val lines = source.getLines()
    //println("The number of rows before cleaning %d ", lines.length)
    val data = lines.drop(1)
    val rows  = data.flatMap{ l=> // this is where this one differs from the previous one, flatmap would take in a seq
      val p = l.split(",")
      if(p(6)=="."||p(7)=="."||p(8)==".") Seq.empty
      else
        Seq(TempDataCC(p(0).toInt,p(1).toInt,p(2).toInt,p(3).toInt,toDoubleOrNeg(p(4)),toDoubleOrNeg(p(5)),p(6).toInt,p(7).toInt,p(8).toInt))
    }.toArray
    source.close()

    // to find number of days having more than 1 as precipitation
    // this can be done by filtering but, this is inefficient so
    val moreThan1 = rows.count(_.precip>=1.0)
    println("Number of days with more than 1 pericipitation ", moreThan1)
    println(s"The percent of rainy days is ${moreThan1*100.0/rows.length}%")

    // for all those rainy days, whats the lowest temp
    // filter rainy days and map them by thier high temp and sum them, this is not efficient and more space consuming
    // reduce can be sued to combine but uses only one type
    // but a fold can be sued for more than one type, if it is left or right
    // 0.0 -> 0 is a tuple
    val (rainySum, rainyCount) = rows.foldLeft(0.0 -> 0)
                                  { case ((sum, count),td) =>
                                        if(td.precip<1.0) (sum, count)
                                        else (sum+td.maxT,count+1)
                                  }
    println(s"Average temp on a rainy day is  ${rainySum/rainyCount}")
  }
}

/*
(Number of days with more than 1 pericipitation ,575)
The percent of rainy days is 1.3928926140355127%
Average temp on a rainy day is  69.30260869565217
 */

