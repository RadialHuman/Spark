name := "spark_scala_tryout1"

version := "1.0"

scalaVersion := "2.11.11"


libraryDependencies ++= Seq(
  //"org.apache.spark" %% "spark-streaming" % "",
  "org.apache.spark" %% "spark-sql" % "2.1.0",
  //"org.apache.spark" %% "spark-streaming-twitter" %
  "org.apache.spark" %% "spark-mllib" % "2.1.0"
)