import org.apache.spark.{SparkConf, SparkContext}
// 19. DoubleRDDFunctions in Spark (using Scala)
// Double type RDD
// has spl statistical functions

case class TempData8(day:Int, jd:Int, month:Int, year:Int, precip:Double, snow:Double, avgT:Int, maxT:Int, minT:Int)


object rdd5 {

  def toDoubleOrNeg(s:String):Double={
    try{
      s.toDouble
    }
    catch{
      case  _:NumberFormatException => -1
    }
  }


  def main(s: Array[String]): Unit = {

    val conf = new SparkConf().setAppName("rdd1")
      .setMaster("local[*]")
    val sc = new SparkContext(conf)


    val data = sc.textFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv").filter(!_.contains("Day "))

    val rows = data.flatMap { l =>
      val p = l.split(",")
      if (p(6) == "." || p(7) == "." || p(8) == ".") Seq.empty
      else
        Seq(TempDataCC(p(0).toInt, p(1).toInt, p(2).toInt, p(3).toInt, rdd5.toDoubleOrNeg(p(4)), rdd5.toDoubleOrNeg(p(5)), p(6).toInt, p(7).toInt, p(8).toInt))
    }.cache()

     println("Std dev of high: "+rows.map(_.maxT).stdev())
    println("Stats \n"+rows.map(_.precip).stats())

    //rows.collect() foreach println
  }
}

// there is std, variance, sum, approximation and mean
// approx for huge dataset for not true value
// stats is shortcut for mean and std, goes thru rdd only once
// histogram with bin or bucket range with different size

/*
Std dev of high: 25.766705568331016

Stats
(count: 41281, mean: 0.015025, stdev: 0.333867, max: 5.400000, min: -1.000000)
 */