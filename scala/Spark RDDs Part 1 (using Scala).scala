import TempData5.toDoubleOrNeg
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

// 14. Spark RDDs Part 1 (using Scala)
// RDD usage has decreased due to rise of spark SQL, df, datasets in spark 2
// RDD can still be a nice basic start
// application to read files in spark, need spark core
// tranform and action

object rdd1{
  def main(s:Array[String]): Unit ={
    // usually in REPL the sc is made for us, but here we need to crete it
    // configuration has to be first set
    val conf = new SparkConf().setAppName("rdd1")// this is enough for a spark submit, but for local
                                .setMaster("local[*]") // signifies standalone spark
    val sc = new SparkContext(conf) // its set now


    val data = sc.textFile("E:\\Concepts\\Spark\\Data_sets\\MN212142_9392.csv").filter(!_.contains("Day "))
    // this filter is so that later there is no error as this column of ints contain string in the header
    // println(d) would not have worked if this was a large dataset
    // also d is a RDD
   // val data = lines.drop(1) *** RDD do not have a drop method in them ***
    val rows  = data.flatMap{ l=> // this is where this one differs from the previous one, flatmap would take in a seq
      val p = l.split(",")
      if(p(6)=="."||p(7)=="."||p(8)==".") Seq.empty
      else
        Seq(TempDataCC(p(0).toInt,p(1).toInt,p(2).toInt,p(3).toInt, TempData5.toDoubleOrNeg(p(4)),TempData5.toDoubleOrNeg(p(5)),p(6).toInt,p(7).toInt,p(8).toInt))
    }
    //d.take(10) foreach println
    rows.take(10) foreach println // nothin will be returned
    // transform are lazy like map which returns , it just scheduled
    // to get output, use methods that do not return other rdds
    // they are called actions like take
    println(rows.count())

  }
}

/*

TempDataCC(1,335,12,1895,0.0,0.0,12,26,-2)
TempDataCC(2,336,12,1895,0.0,0.0,-3,11,-16)
TempDataCC(3,337,12,1895,0.0,0.0,6,11,0)
TempDataCC(4,338,12,1895,0.0,0.0,2,12,-9)
TempDataCC(5,339,12,1895,0.0,0.0,19,26,11)
TempDataCC(6,340,12,1895,0.0,0.0,32,35,28)
TempDataCC(7,341,12,1895,0.0,0.0,15,31,-2)
TempDataCC(8,342,12,1895,0.0,0.0,6,21,-10)
TempDataCC(9,343,12,1895,0.0,0.0,8,28,-12)
TempDataCC(10,344,12,1895,0.0,0.0,30,40,20)

41281

 */