import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
/* 28 and 29. Spark SQL Part 1 and 2 (using Scala)
 problems with RDD:
The optimization has to be done by the prograammer, rdd can hold any data and the functions are arbitrary lambda
Thus no automatic optimization, as spark doesnknow hwta u are gonna do in future
Research in DB has allowed SSQL to get optimization power
Originally it had DataFrame which was to replcement for RDD
Now there is DataSet
TO start with DataFrames
has rows which has different types unline columns
Scala approaches with type safe and this is different
Dataframe and DataSet acts like sql
This requires spark-session instead of context
but it runs on spark context
*/


object sql4 {
  def main(s: Array[String]): Unit = {

    //  val conf = new SparkConf().setAppName("rdd1").setMaster("local[*]") not anymore
    val spark = SparkSession.builder().master("local[*]").appName("ssql").getOrCreate()
    import spark.implicits._ // useful stuff

    spark.sparkContext.setLogLevel("WARN") // less verbose
    // data loading now from https://www1.ncdc.noaa.gov/pub/data/ghcn/daily/
    // dataframe reader, hs many type file reader, many options too
    val data17 = spark.read.csv("E:\\Concepts\\Spark\\Data_sets\\2017.csv")
    data17.show() // to show the head
    data17.schema.printTreeString() // info on daatframe, were everything is string by default

    // declaring them as numbers or date or any other type is in the next code

    spark.stop()


  }
}


  /*
  +-----------+--------+----+---+----+----+---+----+
|        _c0|     _c1| _c2|_c3| _c4| _c5|_c6| _c7|
+-----------+--------+----+---+----+----+---+----+
|US1MNHN0184|20170101|PRCP|  0|null|null|  N|null|
|US1MNHN0184|20170101|SNOW|  0|null|null|  N|null|
|US1MNHN0184|20170101|SNWD|274|null|null|  N|null|
|US1MNWR0029|20170101|PRCP|  0|null|null|  N|null|
|CA1MB000296|20170101|PRCP|  0|null|null|  N|null|
|ASN00015643|20170101|TMAX|274|null|null|  a|null|
|ASN00015643|20170101|TMIN|218|null|null|  a|null|
|ASN00015643|20170101|PRCP|  2|null|null|  a|null|
|US1MNCV0008|20170101|PRCP|  0|null|null|  N|null|
|US1MNCV0008|20170101|SNOW|  0|null|null|  N|null|
|US1MISW0005|20170101|PRCP|  0|null|null|  N|null|
|US1MISW0005|20170101|SNOW|  0|null|null|  N|null|
|US1MISW0005|20170101|SNWD|  0|null|null|  N|null|
|ASN00085296|20170101|TMAX|217|null|null|  a|null|
|ASN00085296|20170101|TMIN|127|null|null|  a|null|
|ASN00085296|20170101|PRCP|  0|null|null|  a|null|
|ASN00085280|20170101|TMAX|215|null|null|  a|null|
|ASN00085280|20170101|TMIN|156|null|null|  a|null|
|ASN00085280|20170101|PRCP|  0|null|null|  a|null|
|US1MAMD0069|20170101|PRCP| 56|null|null|  N|null|
+-----------+--------+----+---+----+----+---+----+
only showing top 20 rows

root
 |-- _c0: string (nullable = true)
 |-- _c1: string (nullable = true)
 |-- _c2: string (nullable = true)
 |-- _c3: string (nullable = true)
 |-- _c4: string (nullable = true)
 |-- _c5: string (nullable = true)
 |-- _c6: string (nullable = true)
 |-- _c7: string (nullable = true)
   */
