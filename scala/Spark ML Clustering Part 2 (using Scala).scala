import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.SparkSession
// import swiftvis2.plotting
// import swiftvis2._
// 48 & 49. Spark ML Clustering Part 2 and 3 (using Scala)

/*
There are two ML libraries in Spark
1. org.apache.spark.ml.evaluation <- works with the updated dataset types
2. org.apache.spark.mllib <- works with RDDs

Not everything frmo RDD has been moved to Datasets

ML types:
1. Supervised
2. Unsupervised
3. Reinforcement

This will have Clustering (Unsupervised)
 - KMeans
 Goal si to cluster different climate regions on globe

 */
case class Station(sid:String, lat:Double, lon: Double, ele:Double, name:String)


object ml2 {//extends JFXApp {
  def main(s:Array[String]):Unit= {

    val spark = SparkSession.builder().master("local[*]").getOrCreate()
    import spark.implicits._

    spark.sparkContext.setLogLevel("WARN")

    // loda in data and cluster it geographically, typed dataset

    val stats = spark.read.textFile("E:\\Concepts\\Spark\\Data_sets\\ghcnd-stations.txt").map{
      line =>
      val id = line.substring(0,11)
      val lat =  line.substring(12,20).trim.toDouble
      val lon = line.substring(21,30).trim.toDouble
      val ele =  line.substring(31,37).trim.toDouble
      val name =  line.substring(41,71)
      Station(id, lat, lon, ele, name)

    }.cache()

    stats.show()

    // to plot them x axis =longitude
    val x  = stats.select('lon).as[Double].collect()
    val y  = stats.select('lat).as[Double].collect()

    // for plotting swiftvis2.plotting <-- skipped
    // Plot.scatterPlot(x,y,title = "Stations",xLable= "Longitude", yLabel = "Latitude", symbolSize = 3, symbolColor =Black.ARGB)
    // FXRenderer(plot,1000,650)

    //*****************************************************************
    // in the new datsst and daatframe there is concept of transformers and estimators
    // trans : will modify df
    // estimators : they fit to data and then do transformation


    // fitting of model to daatset, but the datset has to be in a specific format, which ahs features mentioend
    // which will be a spark vector, which can be obtained by transforming

    val dataFroModel = new VectorAssembler().setInputCols(Array("lat","lon")).setOutputCol("location")
    val statsWithLoc = dataFroModel.transform(stats)
    statsWithLoc.show()


    val kMeans = new KMeans().setK(2000).setFeaturesCol("location")
    val statsClusterModel = kMeans.fit(statsWithLoc)
    // this gives a model, which has to be used to transform the data
    //*************************************************************
    val clusterdOuput = statsClusterModel.transform(statsWithLoc)
    clusterdOuput.show()

    println(kMeans.explainParams())  // kinda like help in python

    spark.stop()
  }
}


/*
No graph as no jfx installed

+-----------+-------+--------+------+--------------------+
|        sid|    lat|     lon|   ele|                name|
+-----------+-------+--------+------+--------------------+
|ACW00011604|17.1167|-61.7833|  10.1|ST JOHNS COOLIDGE...|
|ACW00011647|17.1333|-61.7833|  19.2|ST JOHNS         ...|
|AE000041196| 25.333|  55.517|  34.0|SHARJAH INTER. AI...|
|AEM00041194| 25.255|  55.364|  10.4|DUBAI INTL       ...|
|AEM00041217| 24.433|  54.651|  26.8|ABU DHABI INTL   ...|
|AEM00041218| 24.262|  55.609| 264.9|AL AIN INTL      ...|
|AF000040930| 35.317|  69.017|3366.0|NORTH-SALANG     ...|
|AFM00040938|  34.21|  62.228| 977.2|HERAT            ...|
|AFM00040948| 34.566|  69.212|1791.3|KABUL INTL       ...|
|AFM00040990|   31.5|   65.85|1010.0|KANDAHAR AIRPORT ...|
|AG000060390|36.7167|    3.25|  24.0|ALGER-DAR EL BEID...|
|AG000060590|30.5667|  2.8667| 397.0|EL-GOLEA         ...|
|AG000060611|  28.05|  9.6331| 561.0|IN-AMENAS        ...|
|AG000060680|   22.8|  5.4331|1362.0|TAMANRASSET      ...|
|AGE00135039|35.7297|    0.65|  50.0|ORAN-HOPITAL MILI...|
|AGE00147704|  36.97|    7.79| 161.0|ANNABA-CAP DE GAR...|
|AGE00147705|  36.78|    3.07|  59.0|ALGIERS-VILLE/UNI...|
|AGE00147706|   36.8|    3.03| 344.0|ALGIERS-BOUZAREAH...|
|AGE00147707|   36.8|    3.04|  38.0|ALGIERS-CAP CAXIN...|
|AGE00147708|  36.72|    4.05| 222.0|TIZI OUZOU       ...|
+-----------+-------+--------+------+--------------------+
only showing top 20 rows

+-----------+-------+--------+------+--------------------+------------------+
|        sid|    lat|     lon|   ele|                name|          location|
+-----------+-------+--------+------+--------------------+------------------+
|ACW00011604|17.1167|-61.7833|  10.1|ST JOHNS COOLIDGE...|[17.1167,-61.7833]|
|ACW00011647|17.1333|-61.7833|  19.2|ST JOHNS         ...|[17.1333,-61.7833]|
|AE000041196| 25.333|  55.517|  34.0|SHARJAH INTER. AI...|   [25.333,55.517]|
|AEM00041194| 25.255|  55.364|  10.4|DUBAI INTL       ...|   [25.255,55.364]|
|AEM00041217| 24.433|  54.651|  26.8|ABU DHABI INTL   ...|   [24.433,54.651]|
|AEM00041218| 24.262|  55.609| 264.9|AL AIN INTL      ...|   [24.262,55.609]|
|AF000040930| 35.317|  69.017|3366.0|NORTH-SALANG     ...|   [35.317,69.017]|
|AFM00040938|  34.21|  62.228| 977.2|HERAT            ...|    [34.21,62.228]|
|AFM00040948| 34.566|  69.212|1791.3|KABUL INTL       ...|   [34.566,69.212]|
|AFM00040990|   31.5|   65.85|1010.0|KANDAHAR AIRPORT ...|      [31.5,65.85]|
|AG000060390|36.7167|    3.25|  24.0|ALGER-DAR EL BEID...|    [36.7167,3.25]|
|AG000060590|30.5667|  2.8667| 397.0|EL-GOLEA         ...|  [30.5667,2.8667]|
|AG000060611|  28.05|  9.6331| 561.0|IN-AMENAS        ...|    [28.05,9.6331]|
|AG000060680|   22.8|  5.4331|1362.0|TAMANRASSET      ...|     [22.8,5.4331]|
|AGE00135039|35.7297|    0.65|  50.0|ORAN-HOPITAL MILI...|    [35.7297,0.65]|
|AGE00147704|  36.97|    7.79| 161.0|ANNABA-CAP DE GAR...|      [36.97,7.79]|
|AGE00147705|  36.78|    3.07|  59.0|ALGIERS-VILLE/UNI...|      [36.78,3.07]|
|AGE00147706|   36.8|    3.03| 344.0|ALGIERS-BOUZAREAH...|       [36.8,3.03]|
|AGE00147707|   36.8|    3.04|  38.0|ALGIERS-CAP CAXIN...|       [36.8,3.04]|
|AGE00147708|  36.72|    4.05| 222.0|TIZI OUZOU       ...|      [36.72,4.05]|
+-----------+-------+--------+------+--------------------+------------------+
only showing top 20 rows

+-----------+-------+--------+------+--------------------+------------------+----------+
|        sid|    lat|     lon|   ele|                name|          location|prediction|
+-----------+-------+--------+------+--------------------+------------------+----------+
|ACW00011604|17.1167|-61.7833|  10.1|ST JOHNS COOLIDGE...|[17.1167,-61.7833]|       547|
|ACW00011647|17.1333|-61.7833|  19.2|ST JOHNS         ...|[17.1333,-61.7833]|       547|
|AE000041196| 25.333|  55.517|  34.0|SHARJAH INTER. AI...|   [25.333,55.517]|       251|
|AEM00041194| 25.255|  55.364|  10.4|DUBAI INTL       ...|   [25.255,55.364]|       251|
|AEM00041217| 24.433|  54.651|  26.8|ABU DHABI INTL   ...|   [24.433,54.651]|       251|
|AEM00041218| 24.262|  55.609| 264.9|AL AIN INTL      ...|   [24.262,55.609]|       251|
|AF000040930| 35.317|  69.017|3366.0|NORTH-SALANG     ...|   [35.317,69.017]|       242|
|AFM00040938|  34.21|  62.228| 977.2|HERAT            ...|    [34.21,62.228]|        20|
|AFM00040948| 34.566|  69.212|1791.3|KABUL INTL       ...|   [34.566,69.212]|       242|
|AFM00040990|   31.5|   65.85|1010.0|KANDAHAR AIRPORT ...|      [31.5,65.85]|       325|
|AG000060390|36.7167|    3.25|  24.0|ALGER-DAR EL BEID...|    [36.7167,3.25]|       537|
|AG000060590|30.5667|  2.8667| 397.0|EL-GOLEA         ...|  [30.5667,2.8667]|       802|
|AG000060611|  28.05|  9.6331| 561.0|IN-AMENAS        ...|    [28.05,9.6331]|       751|
|AG000060680|   22.8|  5.4331|1362.0|TAMANRASSET      ...|     [22.8,5.4331]|       532|
|AGE00135039|35.7297|    0.65|  50.0|ORAN-HOPITAL MILI...|    [35.7297,0.65]|       296|
|AGE00147704|  36.97|    7.79| 161.0|ANNABA-CAP DE GAR...|      [36.97,7.79]|       449|
|AGE00147705|  36.78|    3.07|  59.0|ALGIERS-VILLE/UNI...|      [36.78,3.07]|       537|
|AGE00147706|   36.8|    3.03| 344.0|ALGIERS-BOUZAREAH...|       [36.8,3.03]|       537|
|AGE00147707|   36.8|    3.04|  38.0|ALGIERS-CAP CAXIN...|       [36.8,3.04]|       537|
|AGE00147708|  36.72|    4.05| 222.0|TIZI OUZOU       ...|      [36.72,4.05]|       537|
+-----------+-------+--------+------+--------------------+------------------+----------+
only showing top 20 rows

featuresCol: features column name (default: features)
initMode: The initialization algorithm. Supported options: 'random' and 'k-means||'. (default: k-means||)
initSteps: The number of steps for k-means|| initialization mode. Must be > 0. (default: 2)
k: The number of clusters to create. Must be > 1. (default: 2, current: 2000)
maxIter: maximum number of iterations (>= 0) (default: 20)
predictionCol: prediction column name (default: prediction)
seed: random seed (default: -1689246527)
tol: the convergence tolerance for iterative algorithms (>= 0) (default: 1.0E-4)

 */

