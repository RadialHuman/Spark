# Spark

Spark using R, Python and Scala

R -> SparklyR in R studio

Python -> PySpark in Jupyter Notebook

Scala -> In IntelliJ IDEA

Dataset for spark is from 

>> https://download.bls.gov/pub/time.series/la/


>> https://www1.ncdc.noaa.gov/pub/data/ghcn/daily/
